;----------------------------------------------------------------------------
; VDPCOM.ASM  COMANDOS VDP 1998 GEIJER
;----------------------------------------------------------------------------


	psect	code,global	
	global	hmmm10,lmmm10,lmmm20,lmmm23,line0h,line0v,hmmv0,lmmm30

MACRO	STATUS,REG   
	LD	A,REG
	OUT	(99h),A
	LD	A,8Fh
	OUT	(99h),A
	IN	A,(99h)
ENDM

	psect	code
	
	;*****************************************************************
	; WAIT.VDP    
	;   ESPERA FIN DE COMANDO   (Uso Interno) 
	;*****************************************************************

WAIT_VDP:	STATUS	2
	AND	1
	JR	NZ,WAIT_VDP
	STATUS	0	; esto es para la BIOS       
	RET		; es indispensable (si no se cuelga) 

	;***************************************************************** 
	; HMMM10  Copy VRAM a VRAM alta velocidad    
	;   OJO1!  >> POSITIVO : DIX=0, DIY=0 =>  DERECHA Y ABAJO <<    
	;       VRAM (H,L)-(H+D,L+E) ---> VRAM (B,C)    
	;   OJO2! >> VALE SOLO DE PAGINA 1 A PAGINA 0 !!!! <<    
	;***************************************************************** 
hmmm10: DI
	PUSH	BC
	CALL	WAIT_VDP
	LD	A,32
	OUT	(99h),A
	LD	A,80H+17
	OUT	(99h),A ;R#17 := 32	  
	XOR	A
	LD	C,9Bh
	OUT	(C),H	;SX  32,33       
	OUT	(9Bh),A
	OUT	(C),L	;SY  34,35       
	LD	A,1	;PAG 1    
	OUT	(9Bh),A
	OUT	(C),B	;DX  36,37       
	XOR	A
	OUT	(9Bh),A
	POP	BC
	LD	A,C
	OUT	(9Bh),A ;DY  38,39	 
	XOR	A
	LD	C,9Bh
	OUT	(9Bh),A
	OUT	(C),D	;NX  40,41       
	OUT	(9Bh),A
	OUT	(C),E	;NY  42,43       
	OUT	(9Bh),A
	OUT	(9Bh),A ;dummy (44)	  
	OUT	(9Bh),A ;DIX y DIY 45	    
	LD	A,11010000B	;HMMM	    
	OUT	(9Bh),A
	EI
	RET

	;***************************************************************** 
	; LMMM10  mueve de VRAM a VRAM con op log    
	;  OJO! >>> POSITIVO: DIX=0, DIY=0 , Y SOLO DE PAGINA 1 A PAGINA 0
	;    VRAM (H,L)-(H+D,L+E) ---> VRAM (B,C) (logi-OP : A)    
	;***************************************************************** 
lmmm10: DI
	PUSH	AF	; Guarda log-op       
	PUSH	BC
	CALL	WAIT_VDP
	LD	A,32
	OUT	(99h),A
	LD	A,80H+17
	OUT	(99h),A ;R#17 := 32	  
	XOR	A
	LD	C,9Bh
	OUT	(C),H	;SX	  
	OUT	(9Bh),A       
	OUT	(C),L	;SY	  
	LD	A,1	; PAG 1
	OUT	(9Bh),A
	OUT	(C),B	;DX	  
	XOR	A
	OUT	(9Bh),A
	POP	BC
	LD	A,C
	OUT	(9Bh),A ;DY	   
	XOR	A
	LD	C,9Bh
	OUT	(9Bh),A
	OUT	(C),D	;NX	   
	OUT	(9Bh),A
	OUT	(C),E	;NY	   
	OUT	(9Bh),A
	OUT	(9Bh),A ;dummy	      
	OUT	(9Bh),A ;DIX and DIY	    
	POP	AF	;A := LOGICAL OPERATION        
	OR	10010000B	;LMMM command	      
	OUT	(9Bh),A
	EI
	RET
	
	;***************************************************************** 
	; LMMM30  mueve de VRAM a VRAM con op log    
	;  OJO! >>> POSITIVO: DIX=0, DIY=0 , Y SOLO DE PAGINA 3 A PAGINA 0
	;    VRAM (H,L)-(H+D,L+E) ---> VRAM (B,C) (logi-OP : A)    
	;***************************************************************** 
lmmm30: DI
	PUSH	AF	; Guarda log-op       
	PUSH	BC
	CALL	WAIT_VDP
	LD	A,32
	OUT	(99h),A
	LD	A,80H+17
	OUT	(99h),A ;R#17 := 32	  
	XOR	A
	LD	C,9Bh
	OUT	(C),H	;SX	  
	OUT	(9Bh),A       
	OUT	(C),L	;SY	  
	LD	A,3	; PAG 3
	OUT	(9Bh),A
	OUT	(C),B	;DX	  
	XOR	A
	OUT	(9Bh),A
	POP	BC
	LD	A,C
	OUT	(9Bh),A ;DY	   
	XOR	A
	LD	C,9Bh
	OUT	(9Bh),A
	OUT	(C),D	;NX	   
	OUT	(9Bh),A
	OUT	(C),E	;NY	   
	OUT	(9Bh),A
	OUT	(9Bh),A ;dummy	      
	OUT	(9Bh),A ;DIX and DIY	    
	POP	AF	;A := LOGICAL OPERATION        
	OR	10010000B	;LMMM command	      
	OUT	(9Bh),A
	EI
	RET
	

	;***************************************************************** 
	; LMMM20  mueve de VRAM a VRAM con op log    
	;  OJO! >>> POSITIVO: DIX=0, DIY=0 , Y SOLO DE PAGINA 2 A PAGINA 0
	;    VRAM (H,L)-(H+D,L+E) ---> VRAM (B,C) (logi-OP : A)    
	;***************************************************************** 
lmmm20: DI
	PUSH	AF	; Guarda log-op       
	PUSH	BC
	CALL	WAIT_VDP
	LD	A,32
	OUT	(99h),A
	LD	A,80H+17
	OUT	(99h),A ;R#17 := 32	  
	XOR	A
	LD	C,9Bh
	OUT	(C),H	;SX       
	OUT	(9Bh),A
	OUT	(C),L	;SY       
	LD	A,2	; PAG 2
	OUT	(9Bh),A
	OUT	(C),B	;DX       
	XOR	A
	OUT	(9Bh),A
	POP	BC
	LD	A,C
	OUT	(9Bh),A ;DY	   
	XOR	A
	LD	C,9Bh
	OUT	(9Bh),A
	OUT	(C),D	;NX        
	OUT	(9Bh),A
	OUT	(C),E	;NY        
	OUT	(9Bh),A
	OUT	(9Bh),A ;dummy	      
	OUT	(9Bh),A ;DIX and DIY	    
	POP	AF	;A := LOGICAL OPERATION        
	OR	10010000B	;LMMM command         
	OUT	(9Bh),A
	EI
	RET

	;***************************************************************** 
	; LMMM23  mueve de VRAM a VRAM con op log    
	;  OJO! >>> POSITIVO: DIX=0, DIY=0 , Y SOLO DE PAGINA 2 A PAGINA 3
	;    VRAM (H,L)-(H+D,L+E) ---> VRAM (B,C) (logi-OP : A)    
	;***************************************************************** 
lmmm23: DI
	PUSH	AF	; Guarda log-op       
	PUSH	BC
	CALL	WAIT_VDP
	LD	A,32
	OUT	(99h),A
	LD	A,80H+17
	OUT	(99h),A ;R#17 := 32	  
	XOR	A
	LD	C,9Bh
	OUT	(C),H	;SX	  
	OUT	(9Bh),A
	OUT	(C),L	;SY	  
	LD	A,2	; PAG 2
	OUT	(9Bh),A
	OUT	(C),B	;DX	  
	XOR	A
	OUT	(9Bh),A
	POP	BC
	LD	A,C
	OUT	(9Bh),A ;DY	   
	ld	a,3	;PAG 3
	LD	C,9Bh
	OUT	(9Bh),A
	xor	a
	OUT	(C),D	;NX	   
	OUT	(9Bh),A
	OUT	(C),E	;NY	   
	OUT	(9Bh),A
	OUT	(9Bh),A ;dummy	      
	OUT	(9Bh),A ;DIX and DIY	    
	POP	AF	;A := LOGICAL OPERATION        
	OR	10010000B	;LMMM command	      
	OUT	(9Bh),A
	EI
	RET

	;***************************************************************** 
	; LINE0H  Dibuja una linea "positiva_horizontal" en la pagina 0    
	;   orig (H,L)	Maj=D Min=E ,B (color)	 (logi-OP : A)	  
	;	      .   
	;	    / . 		en este caso	
	;	  /   . Min - paralelo eje Y
	;	/     .
	;   (H,L)......
	;	  Maj - paralelo al eje X
	;
	;***************************************************************** 
line0h: DI
	PUSH	AF	; Guarda log-op       
	CALL	WAIT_VDP
	LD	A,36
	OUT	(99h),A
	LD	A,80H+17
	OUT	(99h),A 	;R#17 := 36
	XOR	A		;PAG 0
	LD	C,9Bh
	OUT	(C),H		;DX #36      
	OUT	(9Bh),A 	;   #37
	OUT	(C),L		;DY #38
	OUT	(9Bh),A 	;   #39
	OUT	(C),D		;NX #40      
	OUT	(9Bh),A 	;   #41
	OUT	(C),E		;NY #42
	OUT	(9Bh),A 	;   #43
	OUT	(C),B		;CLR#44       
	OUT	(9Bh),A 	;DIX and DIY	    
	POP	AF	;A := LOGICAL OPERATION        
	OR	01110000B	;LINE command	      
	OUT	(9Bh),A
	EI
	RET

	;***************************************************************** 
	; LINE0v  Dibuja una linea "positiva_vertical" en la pagina 0    
	;   orig (H,L)	Maj=D Min=E ,B (color)	 (logi-OP : A)	  
	;	      .   
	;	    / . 		en este caso	
	;	  /   . Maj - paralelo eje Y
	;	/     .
	;   (H,L)......
	;	  Min - paralelo al eje X
	;***************************************************************** 
line0v: DI
	PUSH	AF	; Guarda log-op       
	CALL	WAIT_VDP
	LD	A,36
	OUT	(99h),A
	LD	A,80H+17
	OUT	(99h),A 	;R#17 := 36
	XOR	A		;PAG 0
	LD	C,9Bh
	OUT	(C),H		;DX #36      
	OUT	(9Bh),A 	;   #37
	OUT	(C),L		;DY #38
	OUT	(9Bh),A 	;   #39
	OUT	(C),D		;NX #40      
	OUT	(9Bh),A 	;   #41
	OUT	(C),E		;NY #42
	OUT	(9Bh),A 	;   #43
	OUT	(C),B		;CLR#44       
	ld	a,1		; MAJor side parallel to Y axis (!!)
	OUT	(9Bh),A 	;DIX and DIY	    
	POP	AF	;A := LOGICAL OPERATION        
	OR	01110000B	;LINE command	      
	OUT	(9Bh),A
	EI
	RET

	;***************************************************************** 
	; HMMV0  Pinta un rectangulo en la pagina 0    
	;   (H,L)-(H+D,L+E),B (color) 
	;***************************************************************** 
hmmv0:	DI   
	CALL	WAIT_VDP
	LD	A,36
	OUT	(99h),A
	LD	A,80H+17
	OUT	(99h),A 	;R#17 := 36
	XOR	A		;PAG 0
	LD	C,9Bh
	OUT	(C),H		;DX #36      
	OUT	(9Bh),A 	;   #37
	OUT	(C),L		;DY #38
	OUT	(9Bh),A 	;   #39
	OUT	(C),D		;NX #40      
	OUT	(9Bh),A 	;   #41
	OUT	(C),E		;NY #42
	OUT	(9Bh),A 	;   #43
	OUT	(C),B		;CLR#44       
	OUT	(9Bh),A 	;DIX and DIY	    
	LD	a,11000000B	;HMMV command	      
	OUT	(9Bh),A
	EI
	RET


