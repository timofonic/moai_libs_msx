;-----------------------------------------------------------------------------
; FUNCIONES DEL MSX-DOS v1.0 1998 ENRIC GEIJO
;-----------------------------------------------------------------------------

; Macro de llamada
;-----------------

MACRO	dos,func
	ld	c,func
	call	BDOS
ENDM


; Funciones y variables genericas
;---------------------------------
BDOS   EQU     0005h
FCB    EQU     005Ch	 ; FCB por defecto
	

; Funciones DOS 1 utiles
;------------------------

CREAR_FICH   EQU     16h
SET_DMA      EQU     1Ah
ESCR_BLOCK   EQU     26h
CERRAR_FICH  EQU     10h
ABRIR_FICH   EQU     0Fh
LEER_BLOCK   EQU     27h
BORRAR_FICH  EQU     13h
PRINT_STR    EQU     09h
SRCH_FIRST   EQU     11h
SRCH_NEXT    EQU     12h
   

	   
      

