;-----------------------------------------------------------------------------
; MBLOAD.ASM  CARGADOR DE MUSICAS MB ADAPTADO POR GEIJER PARA MoaiTech 1998
;-----------------------------------------------------------------------------
; Requiere MBPLAY.ASM

*Include bios.i 	;originalmente se asumia bios activa (uso desde BASIC)

	psect	code,global
	psect	data,global
	global	BuscaChips,MBSongLoad,MBSampleKitLoad,chips,MBSongFCB

	;---------------------------------------------------------------------
	; Variables globales usadas por MBPLAY.ASM
	;---------------------------------------------------------------------
	psect	data
chips:	defb	0	; originalmente :: equ	   #c000

	;---------------------------------------------------------------------
	; BuscaChips
	;    salida: chips vale 1,2, o 3 . (msx-audio,msx-music,ambos)
	;	     0 si no encontrado; ojo:: se decrementa antes de salir
	;	     y se asume que siempre se encuentra, es decir, si no 
	;	     se encuentra niguno, la salida valdr� cero (!?)
	;			MUCHO OJO CON ESTO EH?!
	;	NOTA :: Como esta funci�n commuta PAG1, evidentemente no 
	;		puede estar en ella si queremos que funcione bien.	  
	;---------------------------------------------------------------------
	psect	code
BuscaChips: 
	XOR	A
	LD	(chips),A
	CALL	SRCFMP
	CALL	SRCAUD
	LD	A,(chips)
	OR	A
	RET	Z
	DEC	A
	LD	(chips),A
	RET

	;---------------------------
	; BUSCA MSX-MUSIC
	;---------------------------

SRCFMP: LD	HL,0FCCAh	; esto inspecciona la pagina 1
	XOR	A		; de todos los slots y subeslots
	LD	B,4		; buscando la cadena 'OPLL'
FMLP2:	PUSH	BC		; al final la deja en el aire; por lo que
	LD	B,4		; hay que utilizar MAPMEM.ASM para colocar
FMLP1:	PUSH	BC		; en ella  lo que nos interese.
	PUSH	AF
	PUSH	HL
	SET	7,A
	LD	H,040h
	bios	024h		;ENASLT
	POP	HL
	PUSH	HL
	LD	A,(HL)
	CP	020h
	CALL	Z,FMTEST
	JP	Z,FMFND
	POP	HL
	POP	AF
	ADD	A,4
	AND	0Fh
	INC	HL
	INC	HL
	INC	HL
	INC	HL
	POP	BC
	DJNZ	FMLP1
	ADD	A,1
	AND	03h
	POP	BC
	DJNZ	FMLP2
	JP	SETBAS
FMTEST:	LD	HL,0401Ch
	LD	DE,FMTEXT
	LD	B,4
FMLP:	LD	A,(DE)
	CP	(HL)
	RET	NZ
	INC	HL
	INC	DE
	DJNZ	FMLP
	CP	A
	RET
FMFND:	POP	HL
	POP	AF
	POP	BC
	POP	BC
	LD	A,(chips)
	SET	1,A
	LD	(chips),A
	LD	A,(07FF6h)	 ; esto creo que inicializa el FM
	OR	1
	LD	(07FF6h),A
SETBAS: ;DI			 ; esto activa el BASIC
	;LD	 A,(0FCC1h)	 ; pero como no me interesa, lo anulo
	;LD	 H,040h 	 ; de momento.
	;bios	 024h
	;EI
	RET
	psect	data
FMTEXT: DEFM	'OPLL'

	;-------------------------
	; BUSCA MSX-AUDIO
	;-------------------------	  
	psect	code
SRCAUD:	IN	A,(0C0h)
	CP	0FFh
	RET	Z
	LD	A,(chips)
	SET	0,A
	LD	(chips),A
	RET

	;---------------------------------------------------------------------
	; MBSongLoad
	;	DE :: Destino RAM, Nombre fichero en FCB+1
	; 
	;---------------------------------------------------------------------
	psect	code
MBSongLoad: 
	PUSH	DE
	CALL	OPENF
	LD	HL,(BLENG)
	POP	DE
	CALL	LOADF
	JP	CLOSEF		; el ret est� en BDOS

	;---------------------------------------------------------------------
	; MBSampleKitLoad
	;      -> Fichero en FCB+1
	;      <- Carga cabecera de 56 bytes y la mueve a smpadr
	;	  Carga por separado dos bloques de 16Kb en la p�gina 2
	;	  y los mueve a la AudioRAM via ports IO
	;	i.e precisa de la p�gina 2, la sobreescribe y la deja libre
	;---------------------------------------------------------------------
	psect	code
MBSampleKitLoad:
	CALL	OPENF
	LD	DE,08000H
	LD	HL,56
	CALL	LOADF
	LD	HL,08000H
	LD	DE,smpadr
	LD	BC,56
	LDIR
	LD	A,1
	LD	(SAMPDT),A
	LD	HL,04000H
	LD	DE,08000H
	CALL	LOADF
	CALL	MOVSMP
	LD	HL,04000h
	LD	DE,08000H
	CALL	LOADF
	CALL	MOVSMP
	JP	CLOSEF		; el ret est� en bdos
	
	;-----------------------------------
	; MUEVE EL SAMPLEKIT A AUDIORAM
	;-----------------------------------
	psect	data
SAMPDT: DEFB	1
	psect	code
MOVSMP:	LD	A,(SAMPDT)
	XOR	1
	LD	(SAMPDT),A
	OR	A
	EX	AF,AF'
	CALL	Z,MOVSM2
	EX	AF,AF'
	CALL	NZ,MOVSM3
	DI
	LD	A,4
	OUT	(0C0h),A
	LD	A,060h
	OUT	(0C1h),A
	LD	BC,04000h
	LD	HL,08000h
MOVSM6:	LD	A,0Fh
	OUT	(0C0h),A
	LD	A,(HL)
	OUT	(0C1h),A
	LD	A,4
	OUT	(0C0h),A
	LD	A,080h
	OUT	(0C1h),A
	INC	HL
	DEC	BC
	LD	A,B
	OR	C
	JR	Z,MOVSM4
MOVSM5:	IN	A,(0C0h)
	BIT	7,A
	JR	Z,MOVSM5
	BIT	4,A
	JR	Z,MOVSM6
MOVSM4:	LD	A,4
	OUT	(0C0h),A
	LD	A,078h
	OUT	(0C1h),A
	LD	A,080h
	OUT	(0C1h),A
	LD	A,7
	OUT	(0C0h),A
	LD	A,1
	OUT	(0C1h),A
	EI
	RET
MOVSM3:	LD	HL,SAMPD3
	JR	MOVSM7
MOVSM2:	LD	HL,SAMPD2
MOVSM7:	LD	B,10
MOVSM8:	LD	A,(HL)
	OUT	(0C0h),A
	INC	HL
	LD	A,(HL)
	OUT	(0C1h),A
	INC	HL
	DJNZ	MOVSM8
	RET

	psect	data
SAMPD2: DEFB	004h,078h,004h,080h,007h,001h,007h,060h,008h,000h
	DEFB	009h,000h,00ah,000h,00bh,0ffh,00ch,00fh,010h,0f0h
SAMPD3: DEFB	004h,078h,004h,080h,007h,001h,007h,060h,008h,000h
	DEFB	009h,000h,00ah,010h,00bh,0ffh,00ch,01fh,010h,0f0h

;-----------------------------------------------------------------------------
;   FUNCIONES AUXILIARES DE ACCESO A DISCO
;-----------------------------------------------------------------------------
	
BDOS	EQU	0005h	; originalmente preparado para DISK-BASIC :: 0F37Dh
SETDMA	EQU	26
READ	EQU	39
OPEN	EQU	15
CLOSE	EQU	16

	psect	code
OPENF:	LD	DE,FCB
	LD	C,OPEN
	CALL	BDOS
	LD	HL,1
	LD	(GROOT),HL
	DEC	HL
	LD	(BLOK),HL
	LD	(BLOK+2),HL
	RET
LOADF:	PUSH	HL
	LD	C,SETDMA
	CALL	BDOS
	LD	DE,FCB
	POP	HL
	LD	C,READ
	JP	BDOS
CLOSEF: LD	DE,FCB
	LD	C,CLOSE
	JP	BDOS

	;---------------------------
	;  FCB
	;---------------------------
	psect	data
MBSongFCB:
FCB:	DEFB	  0
	DEFM	  '???????????'
	DEFW	  0
GROOT:	DEFW	  0
BLENG:	DEFW	  0
	DEFB	  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
BLOK:	DEFW	  0
	DEFW	  0
