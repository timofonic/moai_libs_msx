#
# ZAS-LINK MAKEFILE    1998 ENRIC GEIJO
#
# MoaiTech Internal MapEditor v2.0's makefile 
#	

CLIB = moai01.lib 
COBJ = mappy01.o mappy02.o mapfuncs.o

#   
# update mappy.com							   
#

mappy.com : $(COBJ)
	link -Z -MMAP -Pcode=100h,data,diskbuffer=4000h -C100H -Omappy.com $(COBJ) $(CLIB)  
	
#
# update object files
#

mappy01.o : mappy01.asm  bios.i subbios.i sysvar.i
	ZAS -U -omappy01.o mappy01.asm
	
mappy02.o : mappy02.asm  bios.i subbios.i sysvar.i
	ZAS -U -omappy02.o mappy02.asm

mapfuncs.o : mapfuncs.asm
	ZAS -U -omapfuncs.o mapfuncs.asm 

#
# end of makefile

