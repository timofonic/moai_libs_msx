;-----------------------------------------------------------------------------
; MODULO DE GESTION DE SPRITES 01 (BITMAP)			  1998 GEIJER
;-----------------------------------------------------------------------------

	psect	code,global
	psect	data,global
	global	SetPrototypeXY,SetPrototype8x8,SetPrototype16x16
	global	PutSpriteXY ;,ClearSpriteXY	  ;,ClearSpriteXYFast
;	global	PutSprites8x8,ClearSprites8x8,ClearSprites8x8Fast
;	global	PutSprites16x16,ClearSprites16x16,ClearSprites16x16Fast

	;---------------------------------------------------------------------	      
	; PutSpriteXY
	;	Coloca en p�gina 0 un sprite de tipo XY suavemente
	;
	;	HL : puntero a una estructura del tipo sg:
	;	     numPrototipo:   defb    0	     ; tipo de sprite
	;	     Xpos:	     defb    0
	;	     Ypos:	     defb    0
	;	     Estado:	     defb    0	     ; n�mero de frame en el 
	;					     ; orden establecido en 
	;					     ; DEM004
	;---------------------------------------------------------------------	  
	psect	code
PutSpriteXY:	    
		;--------------------------------
		; calcula coordenadas de origen
		;--------------------------------
	push	hl
	pop	iy			; iy <-- Estructura de visualizaci�n	 
	ld	b,(hl)			; num protoptipo	
	ld	hl,PrototypeXY
	ld	a,b
	or	a
	jr	z,2f			; si es cero, no suma
	dec	b			; siempre suma uno de mas
	ld	de,25			; longitud de la estructura	   
1:	add	hl,de			; 25*num protoripo
	djnz	1b			; 
2:	push	hl 
	pop	ix			; ix <-- Prototipo	  
	ld	bc,4
	add	hl,bc			; al inicio de la tabla de estados
	ld	c,(iy+3)		; estado a visualizar
	ld	b,0
	add	hl,bc			; se coloca en el estado dado
	ld	b,(hl)
	ld	l,(ix+2)		; pos X de la tabla
	ld	a,b
	or	a
	jr	z,2f			; si 0, no hay que sumar
	dec	b			; siempre suma uno de m�s
	ld	e,(ix)			; ancho de prototipo
	ld	d,0
	ld	h,0	   
1:	add	hl,de
	djnz	1b
2:	
	ld	a,l
	ld	(Xorigen),a
	ld	b,h			; recoge el resto
	ld	l,(ix+3)		; posicion Y de la tabla
	ld	a,b
	or	a
	jr	z,2f			; si  0 no hay que sumar
	dec	b
	ld	e,(ix+1)		; alto del prototipo	    
	ld	d,0
	ld	h,0  
1:	add	hl,de
	djnz	1b   
2:
	ld	a,l
	ld	(Yorigen),a						     
		    
		;--------------------------------
		; hace un Copy con TIMP en pagina 0
		;--------------------------------
	ld	a,(Xorigen)
	ld	h,a
	ld	a,(Yorigen)
	ld	l,a
	ld	d,(ix)
	ld	e,(ix+1)
	ld	b,(iy+1)
	ld	c,(iy+2)
	ld	a,8		;TIMP
	call	lmmm30
	ret
	psect	data
Xorigen:	
	defb	0
Yorigen:
	defb	0

	;---------------------------------------------------------------------
	; SetPrototypeXY
	;	A : numero de prototipo, HL : Puntero a estructura prototipo
	;---------------------------------------------------------------------
	psect	code
SetPrototypeXY:
	push	hl
	ld	b,a
	ld	hl,PrototypeXY
	ld	de,25
1:	add	hl,de
	djnz	1b
	ld	d,h
	ld	e,l
	pop	hl
	ld	bc,25
	ldir
	ret	   
      
	;---------------------------------------------------------------------
	; SetPrototype8x8
	;	A : numero de prototipo, HL : Puntero a estructura prototipo
	;---------------------------------------------------------------------
	psect	code
SetPrototype8x8:
	push	hl
	ld	b,a
	ld	hl,Prototype8x8
	ld	de,22
1:	add	hl,de
	djnz	1b
	ld	d,h
	ld	e,l
	pop	hl
	ld	bc,22
	ldir
	ret

	;---------------------------------------------------------------------
	; SetPrototype16x16
	;	A : numero de prototipo, HL : Puntero a estructura prototipo
	;---------------------------------------------------------------------
	psect	code
SetPrototype16x16:
	push	hl
	ld	b,a
	ld	hl,Prototype16x16
	ld	de,22
1:	add	hl,de
	djnz	1b
	ld	d,h
	ld	e,l
	pop	hl
	ld	bc,22
	ldir
	ret



	;---------------------------------------------------------------------
	; TABLAS DE PROTOTIPOS 
	;--------------------------------------------------------------------- 
 
	;-----------------------------------
	; Sprites de dimensiones Arbitrarias
	;-----------------------------------
	; DimX: 	  defb	  16	   ; dimensiones n*8 en pixels
	; DimY: 	  defb	  16
	; PosPatrn:	  defb	  0,0	   ; Xp,Yp en p�gina 3
	; FramesArriba:   defb	  0,0,0     
	; FramesAbajo:	  defb	  0,0,0
	; FramesIzq:	  defb	  0,0,0
	; FramesDerecha:  defb	  0,0,0
	; FramesAccion1:  defb	  0,0,0
	; FramesAccion2:  defb	  0,0,0
	; FramesAccion3:  defb	  0,0,0

	psect	data
PrototypeXY:
	defs	25*16		; 25 bytes por 16 prototipos

	;-----------------------------------
	; Sprites de 8x8 
	;-----------------------------------
	; PosPatrn:	  defb	  0	 ; Yp en p�gina 3
	; FramesArriba:   defb	  0,0,0     
	; FramesAbajo:	  defb	  0,0,0
	; FramesIzq:	  defb	  0,0,0
	; FramesDerecha:  defb	  0,0,0
	; FramesAccion1:  defb	  0,0,0
	; FramesAccion2:  defb	  0,0,0
	; FramesAccion3:  defb	  0,0,0

	psect	data
Prototype8x8:
	defs	22*16		; 22 bytes por 16 prototipos

	;-----------------------------------
	; Sprites de 16x16
	;-----------------------------------
	; PosPatrn:	  defb	  0	 ; Yp en p�gina 3
	; FramesArriba:   defb	  0,0,0     
	; FramesAbajo:	  defb	  0,0,0
	; FramesIzq:	  defb	  0,0,0
	; FramesDerecha:  defb	  0,0,0
	; FramesAccion1:  defb	  0,0,0
	; FramesAccion2:  defb	  0,0,0
	; FramesAccion3:  defb	  0,0,0

	psect	data
Prototype16x16:
	defs	22*16		; 22 bytes por 16 prototipos


