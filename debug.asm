;-----------------------------------------------------------------------------
; DEBUG.ASM  MODULO DE DEPURACION			    1998 ENRIC GEIJO
;-----------------------------------------------------------------------------

*Include dos.i

	psect	code,global
	psect	data,global

	global	ViewCPUStatus

	;---------------------------------------------------------------------
	; ViewCPUStatus
	;		Muestra todos los registros de la CPU sin modificar
	;		ninguno de ellos. 
	;---------------------------------------------------------------------
	psect	code
ViewCPUStatus:
	push	iy	; copia para salvar
	push	ix
	push	hl
	push	de
	push	bc
	push	af
	exx
	ex	af,af'
	push	hl
	push	de
	push	bc
	push	af
	exx
	ex	af,af'
	push	iy	; Copia para debug
	push	ix
	push	hl
	push	de
	push	bc
	push	af
	exx
	ex	af,af'
	push	hl
	push	de
	push	bc
	push	af

	pop	de		; AF;
	ld	hl,regafp	 
	ld	b,4
	ld	a,10011001B	; formato
	call	NumToASC	

	pop	de		; BC'
	ld	hl,regbcp	 
	ld	b,4
	ld	a,10011001B	; formato
	call	NumToASC	

	pop	de		; DE'
	ld	hl,regdep
	ld	b,4
	ld	a,10011001B	; formato
	call	NumToASC		
		   
	pop	de		; HL'
	ld	hl,reghlp
	ld	b,4
	ld	a,10011001B	; formato
	call	NumToASC					     
				     
	pop	de		; AF
	ld	hl,regaf	
	ld	b,4
	ld	a,10011001B	; formato
	call	NumToASC	

	pop	de		; BC
	ld	hl,regbc	
	ld	b,4
	ld	a,10011001B	; formato
	call	NumToASC	

	pop	de		; DE
	ld	hl,regde
	ld	b,4
	ld	a,10011001B	; formato
	call	NumToASC		
		   
	pop	de		; HL
	ld	hl,reghl
	ld	b,4
	ld	a,10011001B	; formato
	call	NumToASC					     

	pop	de		; IX
	ld	hl,regix	
	ld	b,4
	ld	a,10011001B	; formato
	call	NumToASC	

	pop	de		; IY
	ld	hl,regiy	
	ld	b,4
	ld	a,10011001B	; formato
	call	NumToASC	

	ld	de,regs
	dos	PRINT_STR
	     
	pop	af
	pop	bc
	pop	de
	pop	hl
	ex	af,af'
	exx
	pop	af
	pop	bc
	pop	de
	pop	hl
	pop	ix
	pop	iy
	ret

	;---------------------------------------------------------------------
	; CADENA A RELLENAR E IMPRIMIR
	;---------------------------------------------------------------------	      
	psect	data
regs:	defm	'AF ::'
regaf:	defm	'0000'
	defm	' BC ::'
regbc:	defm	'0000'
	defm	' DE ::'
regde:	defm	'0000'
	defm	' HL ::'
reghl:	defm	'0000'
	defm	' IX ::'
regix:	defm	'0000'
	defm	' IY ::'
regiy:	defm	'0000'
	defb	10,13
	defm	'AF`::'
regafp: defm	'0000'
	defm	' BC`::'
regbcp: defm	'0000'
	defm	' DE`::'
regdep: defm	'0000'
	defm	' HL`::'
reghlp: defm	'0000'
	defb	10,13
	defm	'$'        
		
;-----------------------------------------------------------------------------
; FUNCIONES AUXILIARES
;-----------------------------------------------------------------------------

	;---------------------------------------------------------------------
	;NumToASC			[ by Nestor Soriano, SD MESXES ]
	;  Conversion de un entero de 16 bits a una cadena de caracteres
	;
	;	Entrada: DE = Numero a convertir
	;		 HL = Buffer para depositar la cadena
	;		 B  = Numero total de caracteres de la cadena
	;		 sin incluir signos de terminacion
	;		 C  = Caracter de relleno
	;		  El numero se justifica a la derecha, y los
	;		   espacios sobrantes se rellenan con el caracter (C).
	;		   Si el numero resultante ocupa mas caracteres que
	;		 los indicados en B, este registro es ignorado
	;		  y la cadena ocupa los caracteres necesarios.
	;		  No se cuenta el caracter de terminacion, "$" o 00,
	;		   a efectos de longitud.
	;		A = &B ZPRFFTTT
	;	 TTT = Formato del numero resultante
	;			0: decimal
	;			1: hexdecimal 
	;			2: hexadecimal, comenzando con "&H"
	;			3: hexadecimal, comenzando con "#"
	;			4: hexadecimal, acabado en "H"
	;			5: binario 
	;			6: binario, comenzando con "&B"
	;			7: binario, acabado en "B"
	;	 R   = Rango del numero
	;			0: 0..65535 (entero sin signo)
	;			1: -32768..32767 (entero en complemento a dos)
	;			Si el formato de salida es binario,
	;			el numero se interpreta como entero de 8 bits
	;			y el rango es 0..255. Es decir, el bit R
	;			y el registro D son ignorados.
	;	FF  = Tipo de finalizacion de la cadena
	;			0: Adicion de un caracter "$"
	;			1: Adicion de un caracter 00
	;			2: Puesta a 1 del 7o bit del ultimo caracter
	;			3: Sin finalizacion especial
	;	P   = Signo "+"
	;			0: No agnadir un signo "+" a los numeros positivos
	;			1: Agnadir un signo "+" a los numeros positivos
	;	Z   = Ceros sobrantes
	;			0: Quitar ceros a la izquierda
	;			1: No quitar ceros a la izquierda
	;
	;Salida: Cadena a partir de (HL)
	;        B = Numero de caracteres de la cadena que forman
	;            el numero, incluyendo el signo y el indicador
	;            de tipo si son generados
	;        C = Numero de caracteres totales de la cadena
	;            sin contar el "$" o el 00 si son generados 
	;        No modifica HL, DE, A, IX, IY ni reg. alt.
	;---------------------------------------------------------------------
	psect	code
NumToASC:	
	push	af
	push	ix
	push	de
	push	hl
	ld	ix,WorkArea
	push	af
	push	af
	and	00000111B
	ld	(ix+0),a	;Tipo
	pop	af
	and	00011000B
	rrca
	rrca
	rrca
	ld	(ix+1),a	;Fin
	pop	af
	and	11100000B
	rlca
	rlca
	rlca
	ld	(ix+6),a	;Banderas: Z(cero), P(signo +), R(rango)
	ld	(ix+2),b	;No. caracteres finales
	ld	(ix+3),c	;Caracter de relleno
	xor	a
	ld	(ix+4),a	;Longitud total
	ld	(ix+5),a	;Longitud del numero
	ld	a,10
	ld	(ix+7),a	;Divisor a 10
	ld	(ix+13),l	;Buffer pasado por el usuario
	ld	(ix+14),h
	ld	hl,BufRut
	ld	(ix+10),l	;Buffer de la rutina
	ld	(ix+11),h

ChkTyp:	ld	a,(ix+0)	;Divisor a 2 o a 16, o dejar a 10
	or	a
	jr	z,Continue1
	cp	5
	jp	nc,IsBin
IsHexa:	ld	a,16
	jr	ActualType
IsBin:	ld	a,2
	ld	d,0
	res	0,(ix+6)	;Si es binario esta entre 0 y 255
ActualType:	
	ld	(ix+7),a
Continue1:	
	ld	a,(ix+0)	;Comprueba si hay que poner "H" o "B"
	cp	7	;al final
	jp	z,PutB
	cp	4
	jr	nz,ChkTyp2
PutH:	ld	a,'H'
	jr	PutHorB
PutB:	ld	a,'B'
PutHorB:	
	ld	(hl),a
	inc	hl
	inc	(ix+4)
	inc	(ix+5)
ChkTyp2:	
	ld	a,d	;Si el numero es 0 nunca se pone signo	
	or	e
	jr	z,NoSign
	bit	0,(ix+6)	;Comprueba rango  
	jr	z,SignPos
CheckSign:	
	bit	7,d
	jr	z,SignPos
SignNeg:	
	push	hl	;Niega el numero  
	ld	hl,0	;Signo=0:sin signo; 1:+; 2:-  
	xor	a
	sbc	hl,de
	ex	de,hl
	pop	hl
	ld	a,2
	jr	EndSign
SignPos:	
	bit	1,(ix+6)
	jr	z,NoSign
	ld	a,1
	jr	EndSign
NoSign:	xor	a
EndSign:	
	ld	(ix+12),a
Continue2:	
	ld	b,4
	xor	a
	cp	(ix+0)
	jp	z,IsDec
	ld	a,4
	cp	(ix+0)
	jp	nc,IsHexa2
IsBin2:	ld	b,8
	jr	IsHexa2
IsDec:	ld	b,5
IsHexa2: 
	nop

	;DIVISION 16 POR 16   
	;Entrada:  DE=dividendo
	;          (IX+7)=divisor
	;Salida:   DE=cociente
	;          A=resto

	push	de
division:	
	push	bc
	push	hl
	ld	a,d
	ld	c,e
	ld	d,0
	ld	e,(ix+7)
div16:	ld	hl,0
	ld	b,16
loop16:	rl	c
	rla
	adc	hl,hl
	sbc	hl,de
	jr	nc,$+3
	add	hl,de
	ccf
	djnz	loop16
	rl	c
	rla
done:	ld	d,a
	ld	e,c
	ld	a,l
	pop	hl
	pop	bc

ChkRest9:	
	cp	10	;Convierte el resto en caracter
	jp	nc,IsGr9
IsLw9:	add	a,'0'
	jr	PutInBuf
IsGr9:	sub	10
	add	a,'A'

PutInBuf:	
	ld	(hl),a	;Pone caracter en buffer
	inc	hl
	inc	(ix+4)
	inc	(ix+5)
	djnz	division
	pop	de

ChkOutZros:	
	bit	2,(ix+6)	;Comprueba si hay que eliminar ceros
	jr	nz,CheckAmp
KillZeros:	
	dec	hl
	ld	b,(ix+5)
	dec	b	;B=no. de digitos a comprobar
ChkforZro:	
	ld	a,(hl)
	cp	'0'
	jr	nz,EndKilling
KillZro:	
	dec	hl
	dec	(ix+4)
	dec	(ix+5)
	djnz	ChkforZro
EndKilling:	
	inc	hl
CheckAmp:	
	ld	a,(ix+0)	;Coloca "#", "&H" o "&B" si es necesario
	cp	2
	jr	z,PutAmpH
	cp	3
	jr	z,PutNsimb
	cp	6
	jr	nz,PutSign
PutAmpB:	
	ld	a,'B'
	jr	PutAmpHorB
PutNsimb:
	   ld	   a,'#'
	ld	(hl),a
	inc	hl
	inc	(ix+4)
	inc	(ix+5)
	jr	PutSign
PutAmpH:	
	ld	a,'H'
PutAmpHorB:	
	ld	(hl),a
	inc	hl
	ld	a,'&'
	ld	(hl),a
	inc	hl
	inc	(ix+4)
	inc	(ix+4)
	inc	(ix+5)
	inc	(ix+5)

PutSign:	
	ld	a,(ix+12)	;Coloca el signo
	or	a
	jr	z,ChkLon
SgnType:	cp	1
	jr	nz,PutNeg
PutPos: ld	a,'+'
	jr	Put
	jr	ChkLon
PutNeg: ld	a,'-'
Put:	ld	(hl),a
	inc	hl
	inc	(ix+4)
	inc	(ix+5)

ChkLon:	ld	a,(ix+2)	;Pone caracteres de relleno si necesario
	cp	(ix+4)
	jp	c,ChkBit7
	jr	z,ChkBit7
AddChars:      
	sub	(ix+4)
	ld	b,a
	ld	a,(ix+3)
Put1Char:	
	ld	(hl),a
	inc	hl
	inc	(ix+4)
	djnz	Put1Char

ChkBit7:      
	;push	 hl		 ;Guardamos pos. final del buffer
	ld	l,(ix+10)	;para poner despues "$" o 0
	ld	h,(ix+11)	;si es necesario
	ld	a,(ix+1)
	cp	2
	jr	nz,Invert
SetBit7:  
	set	7,(hl)	;Si tipo=2, poner a 1 bit 7 1er car. 


Invert:	xor	a	;Invierte la cadena
	push	hl
	ld	(ix+8),a
	ld	a,(ix+4)
	dec	a
	ld	e,a
	ld	d,0
	add	hl,de
	ex	de,hl
	pop	hl	;HL=buffer inicial, DE=buffer final
	ld	a,(ix+4)
	srl	a
	ld	b,a
InvLoop:	
	push	bc
	ld	a,(de)
	ld	b,(hl)
	ex	de,hl
	ld	(de),a
	ld	(hl),b
	ex	de,hl
	inc	hl
	dec	de
	pop	bc
	djnz	InvLoop
	;pop     hl               ;Recuperamos pos. final de buffer
ToUsBuf:	
	ld	l,(ix+10)
	ld	h,(ix+11)
	ld	e,(ix+13)
	ld	d,(ix+14)
	ld	c,(ix+4)
	ld	b,0
	ldir
	ex	de,hl

ChkFin1:	
	ld	a,(ix+1)	;Comprueba si ha de acabar en "$" o en 0 
	and	00000111B
	or	a
	jr	z,PutDollar
	cp	1
	jr	nz,_End
PutZero:	
	xor	a
	jr	Put1
PutDollar:	
	ld	a,'$'
Put1:	ld	(hl),a

_End:	 ld	 b,(ix+5)
	ld	c,(ix+4)
	pop	hl
	pop	de
	pop	ix
	pop	af
	ret

	psect	data
WorkArea:  
	defs	16
BufRut: defs	10



