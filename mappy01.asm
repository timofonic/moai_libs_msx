;-----------------------------------------------------------------------------
; MAPPY01.ASM	1998 ENRIC GEIJO (GEIJER)
;-----------------------------------------------------------------------------
; Generador de Mapas en Screen 5 para uso interno de MoaiTech
;
; MODULO 01 :: INICIALIZACION Y MENU PRINCIPAL
;	       
;	      (Precisa MAPPY02.ASM :: Modulo de funciones de edici�n)  
;
;	NOTA: Esta versi�n NO realiza tests de error de disco. (MUCHO OJO)
;
;-----------------------------------------------------------------------------

*Include sysvar.i
*Include bios.i
*Include subbios.i

	psect code,global
	psect data,global
	psect diskbuffer,global

	global	buffer
		
	psect	diskbuffer
buffer: defs	1024*32 	   ; 32Kb para leer ficheros GE5 
 
	;-----------------------------------
	; INICIALIZACIONES DE MODO 
	;-----------------------------------
	psect	code
	ld	a,5
	ld	(SCRMOD),a
	subbios CHGMOD
	bios	DISSCR
	

	;------------------------------------
	; CARGA FUENTE SYSTEM.FNT A PAG 2
	;------------------------------------	     
	psect	code
	ld	a,2
	ld	(ACPAG),a
	subbios SETPAG	      
	subbios CLRSPR
	psect	data
_font:	defm	'SYSTEM  FNT'
	psect	code
	ld	hl,_font
	call	LeeFILEaBuffer
	call	MueveFNTaVRAM

	;-----------------------------------
	; DATOS DE CONFIGURACION
	;-----------------------------------
	psect	data
setup_data:
fmap:	defs	11	; fichero de bloques
	defs	2	; 10,13
fbloc:	defs	11	; fichero de mapas
	defs	2	; 10,13
fpatt:	defs	11	; fichero de patrones
	defs	5

	;-----------------------------------
	; LECTURA FICHERO DE SETUP 
	;-----------------------------------
	psect	data
_setup: defm	'SETUP   LOG'
	psect	code
	ld	hl,_setup
	ld	de,setup_data
	call	BinaryFileRead					   
	
	;-----------------------------------
	; LOG E ID DE MAQUINA
	;-----------------------------------	    
	psect	code
	xor	a
	ld	(ACPAG),a
	ld	(DPPAG),a
	subbios SETPAG
	subbios CLRSPR
	ld	hl,0
	ld	de,255*256+212
	ld	b,1*16+1
	call	hmmv0
	psect	data
_title: defm	'MoaiTech 1998'
	defb	13
	defm	'Editor de Mapas Interno v2.0'
	defb	13,13
	defm	'Mapa    ::'
mapnam: defm	'________.___'       
	defb	13
	defm	'Bloques ::'
blcnam: defm	'________.___'
	defb	13
	defm	'Patrones::'
patnam: defm	'________.___'
	defb	255

	psect	code
	ld	hl,fmap       ; copia los nombres de fichero a su sitio
	ld	de,mapnam
	ld	bc,8
	ldir
	ld	hl,fmap+8
	ld	de,mapnam+9
	ld	bc,3
	ldir
	ld	hl,fbloc       ; copia los nombres de fichero a su sitio
	ld	de,blcnam
	ld	bc,8
	ldir
	ld	hl,fbloc+8
	ld	de,blcnam+9
	ld	bc,3
	ldir
	ld	hl,fpatt       ; copia los nombres de fichero a su sitio
	ld	de,patnam
	ld	bc,8
	ldir
	ld	hl,fpatt+8
	ld	de,patnam+9
	ld	bc,3
	ldir			    
	ld	de,0
	ld	hl,_title		 
	call	PrintStrFont	    
	bios	ENASCR
	call	waitSPC

	;------------------------------------
	; CARGA BLOQUES GRAFICOS A PAG 1
	;------------------------------------			  
	bios	DISSCR
	ld	a,1
	ld	(ACPAG),a
	ld	(DPPAG),a
	subbios SETPAG	      
	subbios CLRSPR
	ld	hl,fbloc     
	call	LeeFILEaBuffer
	call	MueveGE5aVRAM
	  
	;-----------------------------------
	; CARGA PATRONES A RAM
	;-----------------------------------
	psect	code
	ld	hl,fpatt
	ld	de,buffer+16384
	call	BinaryFileRead
		      
	;-----------------------------------
	; INICIALIZA BUFFER DE MAPA
	;-----------------------------------
	psect code
	ld	hl,buffer
1:	ld	(hl),255	; codigo de relleno
	inc	hl
	ld	a,h
	or	l
	cp	80h
	jr	nz,1b

	;-----------------------------------
	; MENU PRINCIPAL 
	;-----------------------------------	   
	psect	code
	xor	a
	ld	(ACPAG),a
	ld	(DPPAG),a
	subbios SETPAG
	ld	d,0		; color 0 = Negro
	ld	e,0
	ld	a,0	
	subbios SETPLT	      
	
mnloop: ld	hl,0
	ld	de,255*256+212
	ld	b,1*16+1
	call	hmmv0
	bios	ENASCR
	ld	hl,menudat
	call	GrMenu	    
	cp	1
	jr	nz,2f
	call	EditMap
	jp	mnloop
2:	cp	2
	jr	nz,3f
	call	Salvar
	jp	mnloop
3:	cp	3
	jr	nz,4f 
	call	Cargar
	jp	mnloop
4:	cp	4
	jr	nz,5f
	call	Inspeccionar
	jp	mnloop
5:	cp	5
	jr	nz,mnloop		 
	bios	TOTEXT
	ret
	
	;-----------------------------------
	; DATOS DE MENU PPAL
	;-----------------------------------
	psect	data
menudat:
	defb	5
	defb	15
	defb	3*16+3
	defb	15
	defb	6
	defb	8
	defw	opt1
	defw	opt2
	defw	opt3
	defw	opt4
	defw	opt5	   
opt1:	defm	' Editar Mapa  '
	defb	255
opt2:	defm	' Salvar Mapa  '                
	defb	255
opt3:	defm	' Cargar Mapa  '
	defb	255
opt4:	defm	' Inspeccionar '
	defb	255
opt5:	defm	' Salir al DOS '         
	defb	255						    


	;---------------------------------------------------------------------
	; Salvar
	;	Salva el mapa en un fichero binario de 16Kb con nombre 
	;	especificado en la configuraci�n
	;---------------------------------------------------------------------		      
	psect	code
Salvar: 
	ld	de,buffer
	ld	hl,fmap
	ld	bc,16384
	call	BinaryFileWrite
	ret	   

	;---------------------------------------------------------------------
	; Cargar
	;	Carga el mapa especificado en la condiguraci�n	
	;---------------------------------------------------------------------		      
	psect	code
Cargar:
	ld	hl,fmap
	call	LeeFILEaBuffer
	ret
		  
;-----------------------------------------------------------------------------			     
; Funciones auxiliares
;-----------------------------------------------------------------------------
	psect	code
LeeFILEaBuffer:
	 ld	 de,buffer
	 call	 BinaryFileRead   
	 ret			 ; paso del test de error

	psect	code	
MueveGE5aVRAM:
	ld	de,0		; inicio pagina actual
	ld	hl,buffer+7	; salta cabecera
	ld	bc,1024*32
	bios	LDIRVM
	subbios RSTPLT		; COLOR=RESTORE 	 
	ret

	psect	code
MueveFNTaVRAM:
	ld	de,0		; inicio pagina actual
	ld	hl,buffer+7	; salta cabecera
	ld	bc,1024*3	; Solo un bloque de 3Kb
	bios	LDIRVM	     
	ret
      
	psect	code
waitSPC:
	bios	CHGET			 
	cp	32
	jr	nz,waitSPC
	ret 
	





