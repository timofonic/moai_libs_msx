;-----------------------------------------------------------------------------
;  MAPFUNCS.ASM   1998 ENRIC GEIJO
;-----------------------------------------------------------------------------

; Funciones para el volcado de Mapas
; Requiere vdpcom.o

	psect code,global
	psect data,global

	global	ViewScr8x8,SetViewPort,SetMapParams,ViewMap8x8
	
	psect   data
ViewPort:
X_i:	defb	0
X_f:    defb    31
Y_i:    defb    0
Y_f:    defb    25
WAncho: defb	31	 ; ancho de la ventana

MapParams:
Ancho:	defb	0
AnchoE: defb	0	; ancho efectivo (Ancho-WAncho)
Inicio: defw	0
Fin:	defw	0  
	
	;--------------------------------------------------------------------
	; SetViewPort
	;       permite ajustar el tama�o de la pantalla del mapa a volcar
	;       Esto permite hacer mapas con pantallas de tama�o variable
	;
	;       B: X_i, C: X_f, D: Y_i, E: Y_f
	;--------------------------------------------------------------------
	psect   code
SetViewPort:
	ld      a,b
	ld      (X_i),a
	ld      a,c
	ld      (X_f),a
	ld      a,d
	ld      (Y_i),a
	ld      a,e
	ld      (Y_f),a
	; calcula WAncho
	ld	a,(X_i)
	ld	b,a
	ld	a,(X_f)
	sub	b
	ld	(WAncho),a
	ret

	;--------------------------------------------------------------------
	; SetMapParams
	;	permite ajustar el tama�o del mapa a volcar para permitir
	;	el funcionamiento de la funcion ViewMap8x8
	;
	;	HL: Inicio, C: Ancho  DE: Final
	;--------------------------------------------------------------------
	psect	code
SetMapParams:
	ld	(Inicio),hl
	ld	a,c
	ld	(Ancho),a
	ld	h,d
	ld	l,e
	ld	(Fin),hl
	;	calcula ancho efectivo
	ld	a,(WAncho)
	ld	b,a
	ld	a,(Ancho)
	sub	b
	ld	(AnchoE),a
	ret

	;--------------------------------------------------------------------
	; ViewScr8x8
	;       vuelca en la pagina cero, tomando los gr�ficos de la pagina
	;       uno, una pantalla de mapa que quepa en el ViewPort.
	;    
	;       HL: ptr a la pantalla
	;	NOTA: Esta funcion solo permite ver pantallas sueltas	     
	;	      y no mapas 2D
	;--------------------------------------------------------------------
	psect   code
ViewScr8x8:
	ld      (ptr),hl
	
	; for (i=Y_i;i=Y_f;i++)
	;       for(j=X_i;j=Y_f;j++)

	ld      a,(Y_i)
	ld	(_i),a
	;{
1:      ld      a,(X_i)
	ld      (j),a
	;{
	; calc coords origen
2:      ld      hl,(ptr)
	ld      a,(hl)
	ld      (bloc),a
	inc	hl
	ld	(ptr),hl
	ld      hl,bloc
	ld      a,(hl)
	rl	a
	rl	a
	rl	a
	and	11111000B
	ld	(col),a
	ld      a,(hl)
	rr	a
	rr	a
	and	00111000B	 
	ld      (fila),a
	; calc coords destino
	ld	a,(_i)	      
	sla     a
	sla     a
	sla     a
	ld      (Yp),a
	ld      a,(j)        
	sla     a
	sla     a
	sla     a
	ld      (Xp),a
	; Copia un bloque
	ld	a,(col)
	ld      h,a
	ld	a,(fila)
	ld      l,a
	ld	de,8*256+8
	ld      a,(Xp)
	ld      b,a
	ld      a,(Yp)
	ld      c,a
	call    hmmm10
	;}
	ld      a,(X_f)
	ld      b,a
	ld      a,(j)
	inc     a
	ld      (j),a
	cp      b
	jr      nz,2b
	;}
	ld      a,(Y_f)
	ld      b,a
	ld	a,(_i)
	inc     a
	ld	(_i),a
	cp      b
	jr	nz,1b
	;}
	ret        
	
	psect   data
_i:	 defb	 0
j:      defb    0
ptr:    defw    0
bloc:   defb    0
fila:   defb    0
col:    defb    0
Xp:     defb    0
Yp:     defb    0

	;--------------------------------------------------------------------
	; ViewMap8x8
	;	vuelca en la pagina cero, tomando los gr�ficos de la pagina
	;	uno, una ventana de mapa que quepa en el ViewPort.
	;      
	;	HL: ptr a la pantalla
	;	NOTA: Esta funcion permite ver mapas 2D previa ejecucion 
	;	      de la funcion SetMapParams
	;--------------------------------------------------------------------
	psect	code
ViewMap8x8:
	ld	(ptr),hl
	       
	; for (i=Y_i;i=Y_f;i++)
	;	for(j=X_i;j=Y_f;j++)

	ld	a,(Y_i)
	ld	(_i),a
	;{
_n:	ld	a,(X_i)
	ld	(j),a
	;{
	; calc coords origen
2:	ld	hl,(ptr)
	ld	a,(hl)
	ld	(bloc),a
	inc	hl
	ld	(ptr),hl
	ld	hl,bloc
	ld	a,(hl)
	rl	a
	rl	a
	rl	a
	and	11111000B
	ld	(col),a
	ld	a,(hl)
	rr	a
	rr	a
	and	00111000B	 
	ld	(fila),a
	; calc coords destino
	ld	a,(_i)	      
	sla	a
	sla	a
	sla	a
	ld	(Yp),a
	ld	a,(j)	     
	sla	a
	sla	a
	sla	a
	ld	(Xp),a
	; Copia un bloque
	ld	a,(col)
	ld	h,a
	ld	a,(fila)
	ld	l,a
	ld	de,8*256+8
	ld	a,(Xp)
	ld	b,a
	ld	a,(Yp)
	ld	c,a
	call	hmmm10
	;}
	ld	a,(X_f)
	ld	b,a
	ld	a,(j)
	inc	a
	ld	(j),a
	cp	b
	jr	nz,2b
	;}

	; salto de Ancho en ptr - WAncho
	ld	a,(AnchoE)
	ld	b,0
	ld	c,a	   
	ld	hl,(ptr)
	add	hl,bc
	ld	(ptr),hl
	
	ld	a,(Y_f)
	ld	b,a
	ld	a,(_i)
	inc	a
	ld	(_i),a
	cp	b
	jr	z,1f
	jp	_n
1:
	;}
	ret	   





	

		

