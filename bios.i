;-------------------------------------------------------------------------------
; BIOS, 1998  ENRIC GEIJO
;------------------------------------------------------------------------------

; MSX BIOS EXCPT TAPE FUNCS

; Macro to use bios from DOS 
;-------------------------------------

MACRO	bios,func
	psect	code
	push	af
	ld	ix,func
	ld	a,(0FCC1h)
	push	af
	pop	iy
	pop	af
	call	001Ch
ENDM

; Simbolos
;----------

;CHKRAM   equ 0000h	 ; (RST 0)
;CGTABL   equ 0004h	 ; puntero a la tabla de patrones de ?caracteres
;VDP_DR   equ 0006h	 ; puerto lectura vdp
;VDP_DW   equ 0007h	 ; puerto escritura
;SYNCHR   equ 0008h	 ; (RST 1)	 
;RDSLT	  equ 000Ch
;CHRGTR   equ 0010h	 ; (RST 2)
;WRSLT	  equ 0014h	 
;OUTDO	  equ 0018h	 ; (RST 3)
;CALSLT   equ 001Ch
;DCOMPR   equ 0020h	 ; (RST 4)
;ENASLT   equ 0024h	 
;GETYPR   equ 0028h	 ; (RST 5)
;CALLF	  equ 0030h	 ; (RST 6) 
;KEYINT   equ 0038h	 ; (RST 7)

; Bytes de informacion sobre el MSX huesped 
; ------------------------------------------
;IDBYT1   equ 002Bh	 ;(2 bytes)
			;&H002B  :bit3=   Soort karaktergenerator: 0=Japans
			;			    1=Internationaal (ASCII)
			;			    2=Koreaans
			;	 bit4+5= Datum formaat: 0= y-m-d
			;				1= m-d-y
			;				2= d-m-y
			;	bit6=	Interrupt freqentie: 1= 50 Hertz
			;				     2= 60 Hertz
			; &H002C  :bit3= Soort toetsenbord: 0= Japans
			;		   1= Internationaal (QWERTY)
			;		   2= Frans (AZERTY)
			;		   3= UK
			;		   4= Duits (DIN)
			;	 bit4/7= Versie van BASIC
IDBYT2	equ  002Dh	; (1 byte)
			; 0= MSX 1
			; 1= MSX 2
			; 2= MSX 2+
			; 3= MSX TURBO R
;IDBYT3  equ 002Eh	 ; (1 byte)
			; 0= Geen MSX MIDI aanwezig
			; 1= MSX MIDI aanwezig
;IDBYT4  equ 002Fh	 ; (1 byte)

;Routines voor I/O initialisatie:
;--------------------------------

;INITIO   equ 003Bh
;INIFNK   equ 003Eh

;Routines om de VDP (Video Display Processor) aan te sturen:
;----------------------------------------------------------

DISSCR	equ 0041h
ENASCR	equ 0044h
;WRTVDP  equ 0047h
;RDVRM	 equ 004Ah
;WRTVRM  equ 004Dh
;SETRD	 equ 0050h
;SETWRT  equ 0053h 
FILVRM	equ 0056h
LDIRMV	equ 0059h
LDIRVM	equ 005Ch
;CHGMOD  equ 005Fh
;CHGCLR  equ 0062h
;NMI	 equ 0066h
;CLRSPR  equ 0069h
;INITXT  equ 006Ch
;INIT32  equ 006Fh
;INIGRP  equ 0072h
;INIMLT  equ 0075h
;SETTXT  equ 0078h
;SETT32  equ 007Bh
;SETGRP  equ 007Eh
;SETMLT  equ 0081h
;CALPAT  equ 0084h
;CALATR  equ 0087h
;GSPSIZ  equ 008Ah
;GRPPRT  equ 008Dh
	  
;Routines om de PSG aan te sturen:
;---------------------------------

;GICINI  equ  0090h
;WRTPSG  equ  0093h
;RDPSG	 equ  0096h
;STRTMS  equ  0099h

;Routines om het CONSOLE/KEYBOARD
;------------------------------------------------------

CHSNS	equ  009Ch
CHGET	equ  009Fh
CHPUT	equ  00A2h
;LPTOUT  equ  00A5h
;LPTSTT  equ  00A8h
;SNVCHR  equ  00ABh
;PINLIN  equ  00AEh
;INLIN	 equ  00B1h
;QINLIN  equ  00B4h
;BREAKX  equ  00B7h
;ISCNTC  equ  00BAh
;CKCNTC  equ  00BDh
;BEEP	 equ  00C0h
CLS	equ  00C3h
POSIT	equ  00C6h
;FNKSB	 equ  00C9h
;ERAFNK  equ  00CCh
;DSPFNK  equ  00CFh
TOTEXT equ  00D2h

;Routines om de JOYSTICK PORT aan te sturen:
;----------------------------------------------

GTSTCK	equ 00D5h
GTTRIG	equ 00D8h
GTPAD	equ 00DBh
GTPDL	equ 00DEh


