;-----------------------------------------------------------------------------
; DIREX v1.0  1998  ENRIC GEIJO
;-----------------------------------------------------------------------------
; Extensi�n del comando DIR de MSXDOS1 para visualizar descripciones 
; extensas de ficheros de texto con una extensi�n determinada y un formato
; prefijado
;-----------------------------------------------------------------------------

*Include dos.i

	psect	code,global
	psect	data,global
	psect	buffer,global

	psect	buffer
buff:	defs	1024

	;-----------------------------------
	; PARA EMPEZAR : DIR *.GDB 
	;-----------------------------------
	psect	code
	ld	de,dir
	dos	SET_DMA

	xor	a
	ld	(FCB),a 	 ; unidad activa, primer byte 0
	ld	hl,files
	ld	de,FCB+1
	ld	bc,11
	ldir
			
	ld	de,FCB		; Busca primer fichero
	dos	SRCH_FIRST
	cp	255
	jr	z,NoFilesFound
	     
MoreFiles:
	ld	hl,filnum
	inc	(hl)
	ld	hl,(filptr)	; incrementa posicion
	ld	bc,33
	add	hl,bc
	ld	(filptr),hl
	ld	d,h
	ld	e,l
	dos	SET_DMA
	ld	de,FCB
	dos	SRCH_NEXT	 
	cp	255
	jr	nz,MoreFiles
	
	;-----------------------------------
	; Impresi�n de la lista .GDB
	;-----------------------------------
	ld	hl,dir
	ld	(filptr),hl

	ld	a,(filnum)
	ld	b,a
buc1:	     
	push	bc
	call	PrintDirEntry
	ld	hl,(filptr)
	ld	bc,33
	add	hl,bc
	ld	(filptr),hl
	pop	bc
	djnz	buc1				    
	ret

	;-----------------------------------
	; No hay ficheros
	;-----------------------------------			    
	psect	code
NoFilesFound:
	ld	de,msg1
	dos	PRINT_STR
	ret
	
	psect	data
msg1:	defm	'No se encontraron ficheros .GDB.$'
msg2:	defm	'hasta 1 OK$'
msg3:	defm	'hasta 2 OK$'
msg4:	defm	'hasta 3 OK$'

	psect	data
files:	defm	'????????GDB'
filptr: defw	dir    
filnum: defb	0
dir:	defs	33*255		;m�ximo de 255 archivos 
					

	
	;---------------------------------------------------------------------
	; PrintDirEntry
	;	Imprime el nombre de un fichero dado el FCB
	;	Lee los primeros 128 bytes y extrae e imprime la siguiente
	;	informaci�n en el formato indicado
	;	Tema {10,13}
	;	Titulo {10,13}
	;	Fecha {10,13}
	;	
	;	HL :: ptr a FCB
	;---------------------------------------------------------------------
	psect	code
PrintDirEntry:
	push	hl
	pop	ix
	push	hl
	ld	(ix+12),32
	ld	(ix+13),'$'      ; formatea sobreescribiendo FCB
	inc	hl
	ld	d,h
	ld	e,l
	dos	PRINT_STR	; imprime nombre de fichero
	     
	pop	hl		; recupera posicion nom fich
	inc	hl
	ld	de,buff
	ld	bc,68
	call	BinaryFileRead
	ld	hl,buff
	ld	a,13
	ld	bc,67
	cpir			; solo imprime un salto de linea !!
	inc	hl
	ld	(hl),'$'
	ld	de,buff
	dos	PRINT_STR				
	ret
		     
	;---------------------------------------------------------------------
	; BinaryFileRead
	;  Lee un fichero binario a DMA 
	;  -> HL nom fich formato fcb, DE  direccion destino, BC num bytes
	;  <- A=#ff,  HL num bytes leidos, A=1 : no se pudo abrir_
	;---------------------------------------------------------------------
	psect	code
BinaryFileRead:
              ; --- Copia nombre de fichero a FCB ---
	PUSH	BC
	PUSH	DE
	XOR	A
	LD	(FCB),A 	 ; unidad activa, primer byte 0
	LD	DE,FCB+1
	LD	BC,11
	LDIR

	      ; --- Abre el fichero ---

	LD	DE,FCB
	dos	ABRIR_FICH
	POP	DE		 ; destino
	OR	A
	JR	Z,_FREAD
	LD	A,1		 ; Codigo de error: No se pudo abrir
	POP	BC
	RET
	      ; --- Lectura del fichero ---
_FREAD:
	dos	SET_DMA 	 ; de=nuevo dma
	LD	HL,1
	LD	(FCB+14),HL	 ; tamanyo registro
	LD	HL,0
	LD	(FCB+33),HL
	LD	(FCB+35),HL	 ; registro aleatorio 0
	LD	DE,FCB		 ; lee bloque ram
	POP	HL
	dos	LEER_BLOCK
	RET


