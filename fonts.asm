;-----------------------------------------------------------------------------
;  FONTS.ASM   1998 ENRIC GEIJO
;-----------------------------------------------------------------------------
; versi�n 2.0  (de momento solo soporta fuentes 8x8)


        psect code,global
	psect data,global
  
        global  SetFont,PutCharFont,PrintStrFont

        ;---------------------------------------
        ; Variables de trabajo de la funciones
        ;---------------------------------------
        psect   data
dstpag: defb    0       ; pagina de destino [0,3] (por defecto 0)
Yorig:  defb    0       ; coordenada Y de la fuente en pag 2 (defecto 0)
LogOP:  defb    8       ; operaci�n l�gica; TIMP por defecto


        ;---------------------------------------------------------------------
        ; SetFont
        ;          Modifica variables de trabajo
        ;
        ;          B: pagina destino
        ;          C: coordenada origen fuente
        ;          A: nueva oplogica
        ;
        ;---------------------------------------------------------------------
        psect   code
SetFont:
        ld      (LogOP),a
        ld      a,b
        ld      (dstpag),a
        ld      a,c
        ld      (Yorig),a
        ret

        ;---------------------------------------------------------------------
        ; PutCharFont
        ;             Imprime un caracter en coordendas X-Y  
        ;
        ;             DE: XX*256+YY
        ;              A: ASCII
        ;
        ;             Se asumen coords 32*24, y no test de error
        ;
        ;---------------------------------------------------------------------
        psect   data
col:    defb    0       ;variables trabajo
fila:   defb    0
Xp:     defb    0
Yp:     defb    0
        psect   code
PutCharFont:
        ; calc coords origen
	sub	32	; minimo 32 (!!)
	push	af
        rl	a
	rl	a
	rl	a
	and	11111000B
	ld	(col),a
        pop     af
        rr	a
	rr	a
	and	00111000B	 
	ld      (fila),a
        ; calc coords destino
	ld	a,e
	sla     a
	sla     a
	sla     a
	ld	(Yp),a
	ld	a,d	   
	sla     a
	sla     a
	sla     a
	ld	(Xp),a
        ; copia caracter
        ld      a,(col)
	ld	h,a		;Xi
        ld      a,(fila)
        ld      b,a             ;A�ade offset fuente
        ld      a,(Yorig)
        add     a,b
	ld	l,a		;Yi
        ld      a,(Xp)
	ld	b,a		;Xd
        ld      a,(Yp)
	ld	c,a		;Yd
        ld      de,8*256+8
        ld      a,(dstpag)
        cp      0
        jr      nz,3f
        ld      a,(LogOP)
        call    lmmm20
        ret
3:      ld      a,(LogOP)
        call	lmmm23
        ret

        ;---------------------------------------------------------------------
        ; PrintStrFont
        ;             Imprime una cadena de caracteres en coordenadas X-Y  
        ;
        ;             DE: XX*256+YY  HL: puntero a string
        ;
        ;             codigos: 13=Enter, 255=FinStr, <32,>128=Imprevisible
        ;
        ;---------------------------------------------------------------------
        psect   data
XY:     defw    0           
ptr:    defw    0
        psect   code
PrintStrFont:
        ld      (ptr),hl
	ld	h,d
	ld	l,e
        ld      (XY),hl
        ld      hl,(ptr)
3:
	ld	a,(hl)	   
	cp	255
        ret     z       ;si no hay 255 no hay retorno (!)
        cp      13
        jr      nz,1f   
        ld      hl,XY   ;hace un enter
	inc	(hl)
	inc	hl
	xor	a
        ld      (hl),a  
	jr	2f
1:
	cp	32	;no imprime tonterias
	jr	c,2f
        ld      hl,(XY)
	ld	d,h
	ld	e,l
        call    PutCharFont
        ;incremento de coordenadas
	ld	hl,XY+1
        ld      a,(hl)  ;X
	inc	a
	ld	(hl),a
	cp	32	;si es 32
        jr      nz,2f
        xor     a
        ld      (hl),a  ;X=0
	dec	hl
	inc	(hl)	;INC Y, y no testea overflow
2:
	ld	hl,(ptr)	; incrementa contador
        inc     hl
        ld      (ptr),hl
        jr      3b



               



