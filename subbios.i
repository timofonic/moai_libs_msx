;-------------------------------------------------------------------------------
; SUBBIOS, 1998 ENRIC GEIJO
;-------------------------------------------------------------------------------

; macro para DOS
;---------------

MACRO subbios,func
	psect	code
	push	af
	ld	ix,func
	ld	a,(0FAF8h)
	push	af
	pop	iy
	pop	af
	call	001Ch
ENDM


;DOGRPH  equ 0085h
;DOLINE  equ 007dh  
;GRPPRT  equ 0089h
;SCALXY  equ 008Dh
;MAPXYC  equ 0091h
;READC	 equ 0095h
;SETATR  equ 0099h
;SETC	 equ 009Dh
;TRIGHT  equ 00A1h
;RIGHTC  equ 00A5h
;TLEFTC  equ 00A9h
;LEFTC	 equ 00ADh
;TDOWNC  equ 00B1h
;DOWNC	 equ 00B5h
;TUPC	 equ 00B9h
;UPC	 equ 00BDh
;SCANR	 equ 00C1h
;SCANL	 equ 00C5h
;NVBXLN  equ 00C9h
;BOXLIN  equ 0081h
;NVBXFL  equ 00CDh 
;DOBOXF  equ 0079h
CHGMOD	equ 00D1h

; Funciones que sustituyen a las de la BIOS de msx1
;--------------------------------------------------

;INITXT  equ 00D5h
;INIT32  equ 00D9h
;INIGRP  equ 00DDh
;INIMLT  equ 00E1h
;SETTXT  equ 00E5h
;SETT32  equ 00E9h
;SETGRP  equ 00EDh
;SETMLT  equ 00F1h
CLRSPR	equ 00F5h
;CALPAT  equ 00F9h
;CALATR  equ 00FDh
;GSPSIZ  equ 0101h

; Funciones modificadas para MSX2  
;----------------------------------

;GETPAT  equ 0105h
;WRTVRM  equ 0109h
;RDVRM	 equ 010Dh
;CHGCLR  equ 0111h
;CLS	 equ 0115h
;CLRTXT  equ 0119h
;DSPFNK  equ 011Dh
;DELLNO  equ 0121h
;INSLNO  equ 0125h
;PUTVRM  equ 0129h
;WRTVDP  equ 012Dh
;VDPSTA  equ 0131h
;KYKLOK  equ 0135h
;PUTCHR  equ 0139h
SETPAG	equ 013Dh

; Funciones de Paleta
;---------------------

;INIPLT  equ 0141h
RSTPLT	equ 0145h	; color=restore
;GETPLT  equ 0149h
SETPLT	equ 014Dh

;BEEP	 equ 017Dh
;PROMPT  equ 0181h
;SDFSCR  equ 0185h
;SETSCR  equ 0189h


; Funciones BIT-BTL
;--------------------

;BLTVV	 equ 0191h  ;Copy VRAM to VRAM
;BLTVM	 equ 0195h  ;Copy Main-RAM to VRAM
;BLTMV	 equ 0199h  ;Copy VRAM to Main-RAM
;BLTVD	 equ 019Dh  ;Copy Diskfile to VRAM
;BLTDV	 equ 01A1h  ;Copy VRAM to Diskfile
;BLTMD	 equ 01A5h  ;Copy Diskfile naar Main-RAM
;BLTDM	 equ 01A9h  ;Copy Main-RAM naar Diskfile


NEWPAD	equ 01ADh
;CHGMDP  equ 01B5h
;RESVI	 equ 01B9h
;KNJPRT  equ 01BDh
;REDCLK  equ 01F5h
;WRTCLK  equ 01F9h


