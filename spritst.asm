;----------------------------------------------------------------------------
; SPRITE.ASM  MODULO DE PRUEBAS CON SPRITE01.ASM      1998 GEIJER
;----------------------------------------------------------------------------

*Include bios.i       
*Include subbios.i
*Include sysvar.i

	psect code,global
	psect data,global
	psect diskbuffer,global
		
	psect	diskbuffer
buffer: defs	1024*32 	   ; 32Kb para leer ficheros GE5 
 
	;-----------------------------------
	; INICIALIZACIONES DE MODO 
	;-----------------------------------
	psect	code
	ld	a,5
	ld	(SCRMOD),a
	subbios CHGMOD
       
	;-----------------------------------
	; CARGA SPRITES A PAG3
	;-----------------------------------
	ld	a,3
	ld	(ACPAG),a
	ld	(DPPAG),a
	subbios SETPAG
	psect	data
_spr:	defm	'POW     SPR'
	psect	code
	ld	hl,_spr
	call	LeeFILEaBuffer					  
	call	MueveGE5aVRAM	     

	;-----------------------------------
	; INICIALIZA PROTOTIPO DE SPRITE
	;-----------------------------------
	psect	data
prototipo:
	defb	25,40,0,0
	defb	0,1,2
	defb	0,0,1
	defb	0,0,1
	defb	0,0,1
	defb	0,0,0
	defb	0,0,0
	defb	0,0,0
	psect	code
	xor	a
	ld	hl,prototipo
	call	SetPrototypeXY
	
	;-----------------------------------
	; COLOCA UN FRAME EN 100,100
	;-----------------------------------
	xor	a
	ld	(ACPAG),a
	ld	(DPPAG),a
	subbios SETPAG
	psect	data
sprtab: defb	0,100,100,0
	psect	code
	ld	hl,sprtab
	call	PutSpriteXY			   
	call	waitSPC
	bios	TOTEXT
	ret	   

;-----------------------------------------------------------------------------			     
; Funciones auxiliares
;-----------------------------------------------------------------------------
	psect	code
LeeFILEaBuffer:
	 ld	de,buffer
	 ld	bc,30*1024	  ; a saco
	 call	BinaryFileRead	 
	 ret			 ; paso del test de error

	psect	code	
MueveGE5aVRAM:
	ld	de,0		; inicio pagina actual
	ld	hl,buffer+7	; salta cabecera
	ld	bc,1024*32
	bios	LDIRVM
	subbios RSTPLT		; COLOR=RESTORE 	 
	ret

	psect	code
MueveFNTaVRAM:
	ld	de,0		; inicio pagina actual
	ld	hl,buffer+7	; salta cabecera
	ld	bc,1024*3	; Solo un bloque de 3Kb
	bios	LDIRVM	     
	ret
      
	psect	code
waitSPC:
	bios	CHGET			 
	cp	32
	jr	nz,waitSPC
	ret 


