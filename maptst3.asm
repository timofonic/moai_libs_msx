;-----------------------------------------------------------------------------
;  MAPTST3.ASM	 1998 ENRIC GEIJO
;-----------------------------------------------------------------------------

*Include sysvar.i
*Include bios.i
*Include subbios.i

	psect code,global
	psect data,global
	psect diskbuffer,global
		
	psect	diskbuffer
buffer: defs	  16384*2	   ; 32Kb para leer ficheros GE5    
 
	psect	code	 
	ld	a,5
	ld	(SCRMOD),a
	subbios CHGMOD

	ld	a,1
	ld	(ACPAG),a
	ld	(DPPAG),a
	subbios SETPAG	      

	call	LeeGC5aBuffer
	call	MueveGE5aVRAM		       
	
	xor	a
	ld	(ACPAG),a
	ld	(DPPAG),a
	subbios SETPAG	      
	call	ShowMap

1:	call	GetTriggerStatus		    
	ld	a,(Strig0)
	cp	255
	jr	nz,1b		     
	
	bios	TOTEXT
	ret	   

; Funciones auxiliares
;----------------------

LeeGC5aBuffer:
	psect	data
_filename:
	defm	'BLOCK   GE5'
	psect	code
	ld	de,buffer
	ld	hl,_filename
	call	BinaryFileRead
	ret			; paso del test de error
       
MueveGE5aVRAM:
	psect	code
	ld	de,0		; inicio pagina actual
	ld	hl,buffer+7	; salta cabecera
	ld	bc,1024*32
	bios	LDIRVM
	subbios RSTPLT		; COLOR=RESTORE 	 
	ret

GetTriggerStatus:
	psect	data
Strig0: defb	  0	   
	psect	code
	ld	a,1h
	bios	GTTRIG
	ld	(Strig0),a
	ret

ShowMap:
	psect	data
map:	defb	0,0,0,0,0,0,0,0,0,0
	defb	0,1,3,2,3,3,3,3,3,0
	defb	0,1,3,2,3,3,3,3,3,0
	defb	0,3,1,2,2,2,2,2,2,0
	defb	0,3,1,3,3,3,3,3,3,0
	defb	0,3,1,1,1,1,1,1,1,0
	defb	0,0,0,0,0,0,0,0,0,0

	psect	code
	ld	bc,10*256+20
	ld	de,10*256+17	  
	call	SetViewPort
	ld	hl,map
	call	ViewMap8x8
	ret
	



