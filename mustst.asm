;----------------------------------------------------------------------------
; MUSICTST.ASM	MODULO DE PRUEBAS MODULOS FM			 1998 GEIJER
;----------------------------------------------------------------------------

*Include bios.i       
*Include subbios.i
*Include sysvar.i
*Include dos.i

	psect code,global
	psect data,global
	psect diskbuffer,global
		
	psect	diskbuffer
buffer: defs	1024*32 	   ; 32Kb para leer ficheros GE5 
 
	psect	code
	in	a,(0a8h)	; guarda selector de slots	     
	push	af
	call	BuscaChips
	di
	pop	af	 
	out	(0a8h),a	; lo deja como estaba (por si acaso)
	ei
	psect	data
musfil: defm	'musica  mbm'
	psect	code
	ld	hl,musfil
	ld	de,MBSongFCB+1
	ld	bc,11
	ldir
	ld	de,8000h
	call	MBSongLoad
	in	a,(0feh)	; coge la p gina activa en PAG3
	ld	hl,8000h
	call	SetSongParams
	ld	a,(chips)
	cp	0
	jr	nz,1f
	ld	de,msg0
	jp	ef
1:	cp	1
	jr	nz,2f		     
	ld	de,msg1
	jp	ef
2:	cp	2
	jr	nz,3f
	ld	de,msg2
	jp	ef	  
3:	cp	3
	jr	nz,ef
	ld	de,msg3
ef:	dos	PRINT_STR
				; aunque la deteccion no funcione bien::
	ld	a,1
	ld	(chips),a	; digo que hay un FMPAC        
	ei
	call	PlaySong
	call	waitSPC
	ret
	
	psect	data
msg0:	defm	'FM no encontrado.$'
msg1:	defm	'MSX-AUDIO detectado.$'
msg2:	defm	'MSX-MUSIC detectado.$'
msg3:	defm	'Detectados MSX-AUDIO y MSX-MUSIC.$'
	     
;-----------------------------------------------------------------------------			     
; Funciones auxiliares
;-----------------------------------------------------------------------------
	psect	code
LeeFILEaBuffer:
	 ld	de,buffer
	 ld	bc,30*1024	  ; a saco
	 call	BinaryFileRead	 
	 ret			 ; paso del test de error

	psect	code	
MueveGE5aVRAM:
	ld	de,0		; inicio pagina actual
	ld	hl,buffer+7	; salta cabecera
	ld	bc,1024*32
	bios	LDIRVM
	subbios RSTPLT		; COLOR=RESTORE 	 
	ret

	psect	code
MueveFNTaVRAM:
	ld	de,0		; inicio pagina actual
	ld	hl,buffer+7	; salta cabecera
	ld	bc,1024*3	; Solo un bloque de 3Kb
	bios	LDIRVM	     
	ret
      
	psect	code
waitSPC:
	bios	CHGET			 
	cp	32
	jr	nz,waitSPC
	ret 


