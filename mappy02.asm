;-----------------------------------------------------------------------------
; MAPPY02.ASM	1998 ENRIC GEIJO (GEIJER)
;-----------------------------------------------------------------------------
; Generador de Mapas en Screen 5 para uso interno de MoaiTech
;
; MODULO 02 :: FUNCIONES DE EDICION Y VISUALIZACION		   
;	       (auxiliar de MAPPY01.ASM)
;-----------------------------------------------------------------------------

*Include sysvar.i
*Include bios.i
*Include subbios.i

	psect code,global
	psect data,global
	psect diskbuffer,global
		 
	global	EditMap,Inspeccionar
	
	;---------------------------------------------------------------------
	; EditMap
	;	Funcion de edicion de mapa de 16Kb de dimensiones (FIJAS)
	;	(5x32)x(4x24)	situado en buffer, haciendo uso de::
	;	256 bloques en pagina 1 situados en (0,0), tabla de patrones
	;	de objetos en buffer+16384	 
	;---------------------------------------------------------------------
	psect	code
EditMap:       
	;-----------------------------------
	; DEFINE PARAMETROS DE MAPA
	;-----------------------------------	    
	ld	bc,1*256+28
	ld	de,1*256+20
	call	SetViewPort
	ld	hl,4*256+4
	ld	de,222*256+158
	ld	b,1*16+1
	ld	c,15
	call	GrWindow
	ld	hl,buffer	; Inicio mapa
	ld	c,5*32		; ancho del mapa (5x32)
	ld	de,buffer+15360 ; fin
	call	SetMapParams
	ld	hl,buffer
	ld	(mapptr),hl
	
	;-----------------------------------
	; INICIALIZACIONES
	;-----------------------------------
	ld	a,11
	ld	(CsrXps),a
	ld	(CsrYps),a	  

	;-----------------------------------
	; IMPRIME PANTALLA ACTUAL
	;-----------------------------------
main1:	ld	hl,(mapptr)
	call	ViewMap8x8	      

	;-----------------------------------
	; VISUALIZA PATRON SELECCIONADO
	;-----------------------------------
main2:	ld	hl,buffer+16384+7	;salta cabecera BIN de fich .pat
	ld	a,(patSEL)
	sla	a
	sla	a
	ld	c,a
	ld	b,0
	add	hl,bc
	ld	(patDAT),hl
	call	ViewSelPat				      

	;-----------------------------------
	; VISUALIZA CURSOR
	;-----------------------------------
main3:	call	ViewCursor

	;-----------------------------------
	; PEQUE�O RETARDO
	;-----------------------------------

main4:	ld	b,15
99:	push	bc
	ld	b,255
999:	djnz	999b 
	pop	bc
	djnz	99b

	;-----------------------------------
	; LEE TECLADO Y ACTUA
	;-----------------------------------	    
main5:	bios	CHGET

		;---------------------------
		; SELECT
		;---------------------------
	cp	24
	jr	nz,10f	      
	bios	CHGET
	cp	32
	jp	c,main4
	sub	32
	ld	(patSEL),a
	call	ViewCursor
	jp	main2				 
	       
		;---------------------------
		; ARRIBA
		;---------------------------		  

10:	cp	30
	jr	nz,4f
	ld	hl,CsrYps
	ld	a,(hl)
	cp	1
	jr	nz,11f
	ld	hl,(mapptr)
	ld	bc,32*5
	scf
	ccf
	sbc	hl,bc
	ld	(mapptr),hl
	jp	main1	     
11:	call	ViewCursor
	ld	hl,CsrYps
	dec	(hl)
	jp	main3		   
		
		;---------------------------
		; DERECHA
		;---------------------------
4:	cp	28
	jr	nz,6f
	ld	hl,CsrXps
	ld	a,(hl)
	cp	26
	jr	nz,22f
	ld	hl,(mapptr)
	inc	hl
	ld	(mapptr),hl
	jp	main1	     
22:	call	ViewCursor
	ld	hl,CsrXps
	inc	(hl)
	jp	main3

		;---------------------------
		; ABAJO
		;---------------------------
6:	cp	31
	jr	nz,7f
	ld	hl,CsrYps
	ld	a,(hl)
	cp	18
	jr	nz,33f
	ld	hl,(mapptr)
	ld	bc,32*5
	add	hl,bc
	ld	(mapptr),hl
	jp	main1	     
33:	call	ViewCursor
	ld	hl,CsrYps
	inc	(hl)
	jp	main3
		
		;---------------------------
		; IZQUIERDA
		;---------------------------
7:	cp	29
	jr	nz,8f
	ld	hl,CsrXps
	ld	a,(hl)
	cp	1
	jr	nz,44f
	ld	hl,(mapptr)
	dec	hl
	ld	(mapptr),hl
	jp	main1	     
44:	call	ViewCursor
	ld	hl,CsrXps
	dec	(hl)
	jp	main3
		
		;---------------------------
		; ESPACIO
		;---------------------------
8:	cp	32
	jr	nz,9f
	; calcula posicion RAM destino y coloca el bloque
	ld	hl,(mapptr)
	ld	a,(CsrYps)
	ld	de,32*5  
	ld	b,a
	dec	b
81:	add	hl,de
	djnz	81b		   
	ld	a,(CsrXps)
	ld	b,0
	ld	c,a
	dec	c
	add	hl,bc
	ld	a,(blocs)
	ld	(hl),a
	inc	hl
	ld	a,(blocs+1)
	ld	(hl),a
	ld	bc,32*5-1
	add	hl,bc
	ld	a,(blocs+2)
	ld	(hl),a
	inc	hl
	ld	a,(blocs+3)
	ld	(hl),a
	jp	main1	    

		;---------------------------
		; ESCAPE
		;---------------------------

9:	cp	27
	jr	nz,10f
	ret

10:	jp	main4

	;	espera tecla
	;	si alfabetica A-Z =>modifica patron
	;	lee cursores
	;	mueve cursor y testea limites
	;	si se rebasan ==>Scroll en esa direccion
		       

	psect	data
patSEL: defb	0
patDAT: defw	0
blocs:	defb	0
	defb	0
	defb	0
	defb	0
CsrXps: defb	11
CsrYps: defb	11

		;-F_AUX-----------------------------
		; visualiza patron seleccionado
		;-F_AUX-----------------------------
	psect	code
ViewSelPat:
	ld	hl,(patDAT)
	ld	a,(hl)
	ld	(bloc),a
	ld	(blocs),a
	inc	hl
	ld	(patDAT),hl
	; coords origen 1
	ld	hl,bloc
	ld      a,(hl)
	rl	a
	rl	a
	rl	a
	and	11111000B
	ld	(col1),a
	ld      a,(hl)
	rr	a
	rr	a
	and	00111000B	 
	ld	(fila1),a
	ld	hl,(patDAT)
	ld	a,(hl)
	ld	(bloc),a
	ld	(blocs+1),a
	inc	hl
	ld	(patDAT),hl
	; coords origen 2
	ld	hl,bloc
	ld	a,(hl)
	rl	a
	rl	a
	rl	a
	and	11111000B
	ld	(col2),a
	ld	a,(hl)
	rr	a
	rr	a
	and	00111000B	 
	ld	(fila2),a
	ld	hl,(patDAT)
	ld	a,(hl)
	ld	(bloc),a
	ld	(blocs+2),a
	inc	hl
	ld	(patDAT),hl
	; coords origen 3
	ld	hl,bloc
	ld	a,(hl)
	rl	a
	rl	a
	rl	a
	and	11111000B
	ld	(col3),a
	ld	a,(hl)
	rr	a
	rr	a
	and	00111000B	 
	ld	(fila3),a
	ld	hl,(patDAT)
	ld	a,(hl)
	ld	(bloc),a
	ld	(blocs+3),a
	inc	hl
	ld	(patDAT),hl
	; coords origen 4
	ld	hl,bloc
	ld	a,(hl)
	rl	a
	rl	a
	rl	a
	and	11111000B
	ld	(col4),a
	ld	a,(hl)
	rr	a
	rr	a
	and	00111000B	 
	ld	(fila4),a	 
	; Copia bloques a destinos fijos
	ld	a,(col1)
	ld      h,a
	ld	a,(fila1)
	ld      l,a
	ld	de,8*256+8
	ld	bc,20*256+165
	call	hmmm10
	ld	a,(col2)
	ld	h,a
	ld	a,(fila2)
	ld	l,a
	ld	de,8*256+8
	ld	bc,28*256+165
	call	hmmm10
	ld	a,(col3)
	ld	h,a
	ld	a,(fila3)
	ld	l,a
	ld	de,8*256+8
	ld	bc,20*256+165+8
	call	hmmm10
	ld	a,(col4)
	ld	h,a
	ld	a,(fila4)
	ld	l,a
	ld	de,8*256+8
	ld	bc,28*256+165+8
	call	hmmm10
	ret

	psect	data			    
bloc:	defb	0
col1:	defb	0
fila1:	defb	0
col2:	defb	0
fila2:	defb	0
col3:	defb	0
fila3:	defb	0
col4:	defb	0
fila4:	defb	0
		;-F_AUX-----------------------------
		; imprime cursor (lmmm10)
		;-F_AUX-----------------------------
	psect	code
ViewCursor:
	; calcula coord destino de cursor
	ld	a,(CsrXps)
	sla	a
	sla	a
	sla	a
	ld	h,a
	ld	a,(CsrYps)
	sla	a
	sla	a
	sla	a
	ld	l,a
	ld	(DstXY),hl		   
	; Copia bloques con coords origen ya calculadas
	ld	a,(col1)
	ld	h,a
	ld	a,(fila1)
	ld	l,a
	ld	de,8*256+8
	ld	bc,(DstXY)
	ld	a,3	; XOR
	call	lmmm10
	ld	a,(col2)
	ld	h,a
	ld	a,(fila2)
	ld	l,a
	ld	de,8*256+8
	ld	bc,(DstXY)
	ld	a,b
	ld	b,8
	add	a,b
	ld	b,a
	ld	a,3	; XOR
	call	lmmm10
	ld	a,(col3)
	ld	h,a
	ld	a,(fila3)
	ld	l,a
	ld	de,8*256+8
	ld	bc,(DstXY)
	ld	a,c
	ld	c,8
	add	a,c
	ld	c,a
	ld	a,3	; XOR
	call	lmmm10
	ld	a,(col4)
	ld	h,a
	ld	a,(fila4)
	ld	l,a
	ld	de,8*256+8
	ld	bc,(DstXY)
	ld	a,c
	ld	c,8
	add	a,c
	ld	c,a
	ld	a,b
	ld	b,8
	add	a,b
	ld	b,a
	ld	a,3	; XOR
	call	lmmm10
	ret
	psect	data
DstXY:	defw	0


	;---------------------------------------------------------------------
	; Inspeccionar
	;	Permite recorrer con los cursores (a scroll bloque a bloque)
	;	el mapa anterior en edici�n  
	;---------------------------------------------------------------------		      
	psect	code
Inspeccionar:
	ld	bc,1*256+28
	ld	de,1*256+20
	call	SetViewPort
	ld	hl,4*256+4
	ld	de,222*256+158
	ld	b,1*16+1
	ld	c,15
	call	GrWindow
	ld	hl,buffer	; Inicio mapa
	ld	c,160	       ; ancho del mapa (5x32)
	ld	de,buffer+15360 ; fin
	call	SetMapParams
	ld	hl,buffer
	ld	(mapptr),hl
5:	ld	hl,(mapptr)
	call	ViewMap8x8	      
	bios	CHGET
	cp	30
	jr	nz,1f
	ld	hl,(mapptr)
	ld	bc,5*32
	scf
	ccf	 
	sbc	hl,bc
	ld	(mapptr),hl
	jr	5b		     
1:	cp	28
	jr	nz,2f
	ld	hl,(mapptr)
	inc	hl		; paso de testear limites; es interno !!
	ld	(mapptr),hl
	jr	5b
2:	cp	31
	jr     nz,3f
	ld	hl,(mapptr)
	ld	bc,5*32
	add	hl,bc
	ld	(mapptr),hl
	jr	5b
3:	cp	29
	jr	nz,4f		     
	ld	hl,(mapptr)
	dec	hl
	ld	(mapptr),hl
	jr	5b
4:	cp	32
	jr	nz,5b
	ret
	
	psect	data
mapptr: defw	0

       
;-----------------------------------------------------------------------------
; FUNCIONES AUXILIARES
;-----------------------------------------------------------------------------
		    
	psect	code
waitSPC:
	bios	CHGET		       
	cp	32
	jr	nz,waitSPC
	ret 
	





