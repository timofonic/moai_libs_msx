;-----------------------------------------------------------------------------
;  GRMENU.ASM  FUNCIONES DE MENU GRAFICO  1998	ENRIC GEIJO
;-----------------------------------------------------------------------------
; Requiere fonts.o

*Include bios.i

	psect	code,global
	global	GrMenu,GrWindow

	;--------------------------------------------------------------------
	; GrWindow
	;		Dibuja una ventana en la pagina 0
	;		HL :: Xi*256+Yi, DE=Ancho*256+Alto
	;		B  :: color ventana  C :: color borde
	;
	;--------------------------------------------------------------------
	psect	code
GrWindow:
	push	hl
	push	de
	push	bc
	call	hmmv0
	pop	bc
	pop	de
	pop	hl
	push	hl
	push	de
	push	bc
	ld	e,0
	ld	b,c
	ld	a,0
	call	line0h
	pop	bc
	pop	de
	pop	hl
	push	hl
	push	de
	push	bc
	ld	d,e
	ld	e,0
	ld	b,c
	ld	a,0
	call	line0v
	pop	bc
	pop	de
	pop	hl
	push	hl
	push	de
	push	bc
	ld	a,l
	add	a,e
	ld	l,a
	ld	e,0
	ld	b,c
	ld	a,0
	call	line0h
	pop	bc
	pop	de
	pop	hl
	ld	a,h
	add	a,d
	ld	h,a
	ld	d,e
	ld	e,0
	ld	b,c
	ld	a,0
	call	line0v
	ret		  
       
	;-------------------------------------------------------------------- 
	; GrMenu
	;		Gestiona un menu en la pagina 0
	;		HL: puntero a estructura de tipo GrMenu
	;		MAX :: 15 OPCIONES (!!)
	;		
	;		hl    :: Num opts
	;		hl+1  :: Long en caracteres de las opciones
	;		hl+2  :: Color de fondo de la ventana
	;		hl+3  :: Color del borde de la ventana
	;		hl+4  :: Xi Menu (32*24)
	;		hl+5  :: Yi Menu (32*24)
	;		hl+6  :: Puntero a cadena primera opcion
	;		hl+7  :: Puntero a cadena segunda opcion
	;		...		     
	;
	;		A :: Devuelve el n�mero de opcion seleccionada
	;
	;--------------------------------------------------------------------
	psect	code		
GrMenu:
	; copia estructura a variables locales
	ld	de,menudata
	ld	bc,6+15*2	 
	ldir
	; calcula coords ventana
	ld	a,(xi)
	sla	a
	sla	a
	sla	a	 
	ld	(XiW),a
	ld	a,(yi)
	sla	a
	sla	a
	sla	a
	ld	(YiW),a
	ld	a,(nopts)
	sla	a
	sla	a
	sla	a
	add	a,16
	ld	(AlW),a
	ld	a,(longc)
	sla	a
	sla	a
	sla	a
	add	a,16
	ld	(AnW),a
	; Imprime ventana
	ld	a,(XiW)
	ld	h,a
	ld	a,(YiW)        
	ld	l,a
	ld	a,(AnW)
	ld	d,a
	ld	a,(AlW)
	ld	e,a
	ld	a,(colfnd)
	ld	b,a
	ld	a,(colbrd)
	ld	c,a
	call	GrWindow		    
	;imprime opciones en posiciones       
	ld	bc,0
	ld	a,2	; OR
	call	SetFont

	ld	a,(xi)
	inc	a
	ld	(xi2),a 	       
	ld	(xi),a
	ld	a,(yi)
	ld	(yi2),a
	ld	a,(nopts)
	ld	b,a
	ld	c,254
1:	inc	c
	inc	c
	push	bc
	ld	a,(yi)
	inc	a
	ld	(yi),a
	ld	e,a
	ld	a,(xi)
	ld	d,a
	ld	hl,opt1
	ld	b,0
	add	hl,bc
	push	hl
	pop	ix
	ld	h,(ix+1)
	ld	l,(ix) 
	call	PrintStrFont
	pop	bc
	djnz	1b			  
	
	;--------------------
	;    BUCLE DE MENU
	;--------------------			       
	ld	a,0
	ld	(SEL),a
5:	call	inv_sel 			    
	;retardo anticursores
6:	ld	b,100
99:	push	bc
	ld	b,255
999:	djnz	999b
	pop	bc
	djnz	99b
	; lee cursores 
1:	bios	CHGET
	cp	30
	jr	nz,2f
	ld	a,(SEL)
	cp	0
	jr	z,6b
	call	uninv_sel
	ld	a,(SEL)
	dec	a
	ld	(SEL),a
	jr	5b			      
2:	cp	31
	jr	nz,3f
	ld	a,(nopts)
	dec	a
	ld	b,a
	ld	a,(SEL)
	cp	b
	jr	z,6b
	call	uninv_sel
	ld	a,(SEL)
	inc	a
	ld	(SEL),a
	jr	5b
3:	cp	32
	jr	nz,1b
	ld	a,(SEL)
	inc	a
	ret
	       
	;-------------------------------
	; invierte opcion seleccionada
	;-------------------------------
inv_sel:
	ld	bc,0
	ld	a,3
	call	SetFont 	      
	   
	ld	a,(yi2)
	ld	b,a
	ld	a,(SEL)
	inc	a
	add	a,b
	ld	e,a
	ld	a,(xi2)
	ld	d,a
	
	ld	hl,opt1
	ld	b,0
	ld	a,(SEL)
	sla	a
	ld	c,a
	add	hl,bc
	push	hl
	pop	ix
	ld	h,(ix+1)
	ld	l,(ix)
	call	PrintStrFont
	ret
	
	;-------------------------------
	; desinvierte opcion seleccionada
	;-------------------------------
uninv_sel:
	ld	bc,0
	ld	a,3	;XOR
	call	SetFont 	      
   
	ld	a,(yi2)
	ld	b,a
	ld	a,(SEL)
	inc	a
	add	a,b
	ld	e,a
	ld	a,(xi2)
	ld	d,a
	
	ld	hl,opt1
	ld	b,0
	ld	a,(SEL)
	sla	a
	ld	c,a
	add	hl,bc
	push	hl
	pop	ix
	ld	h,(ix+1)
	ld	l,(ix)
	call	PrintStrFont
	ret
		
	
				       
	psect	data
menudata:
nopts:	defb	0
longc:	defb	0
colfnd: defb	0
colbrd: defb	0
xi:	defb	0
yi:	defb	0
opt1:	defw	0
opt2:	defw	0
opt3:	defw	0
opt4:	defw	0
opt5:	defw	0
opt6:	defw	0
opt7:	defw	0
opt8:	defw	0
opt9:	defw	0     
opt10:	defw	0
opt11:	defw	0
opt12:	defw	0
opt13:	defw	0
opt14:	defw	0
opt15:	defw	0

XiW:	defb	0
YiW:	defb	0
AlW:	defb	0 
AnW:	defb	0
SEL:	defb	0
xi2:	defb	0
yi2:	defb	0
