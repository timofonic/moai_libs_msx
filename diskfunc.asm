;-----------------------------------------------------------------------------
;  FUNCIONES DE ACCESO A DISCO	  1998	ENRIC GEIJO
;-----------------------------------------------------------------------------

*Include dos.i

	psect	code,global
	global	BinaryFileWrite, BinaryFileRead   

	psect	code		

;****************************************************************************
; BinaryFileWrite
;   Escribe un fichero binario desde la direccion ram especificada
;   -> DE origen ram, HL nombre fichero formato FCB, BC longitud
;   <- A=0 exito, A=#ff error de disco, A=1 no se pudo crear, A=2 no escribir
;   REGS: Todos
;   CALLS: bdos
;   OBS: En caso de existir, borra el fichero y lo reescribe
;****************************************************************************
BinaryFileWrite:		       ; transfiere nombre de fichero a f
              XOR     A
              LD      (FCB),A          ; unidad activa
	      PUSH    BC
	      PUSH    DE
              LD      DE,FCB+1
              LD      BC,11
              LDIR
              LD      DE,FCB
	      dos     ABRIR_FICH       ; si puede abre, si no crea
              OR      A
              JR      Z,_EXISTE

              ; --- si no existe, crea el fichero ---

_NUEVO:       LD      DE,FCB
	      dos     CREAR_FICH
              OR      A
              JR      Z,_ABIERTO
              LD      A,1              ; no se pudo crear
	      POP     DE
	      POP     BC
              RET
_EXISTE:
              ; ---  si existe, lo borra ---

              LD      DE,FCB           ; ???
	      dos     CERRAR_FICH
              LD      DE,FCB
	      dos     BORRAR_FICH
              OR      A
              JR      Z,_NUEVO
              LD      A,3
	      POP     DE
	      POP     BC
              RET

_ABIERTO:
              ; --- Escribe el fichero ---

              POP     DE
	      dos     SET_DMA
              POP     HL               ; params fcb
              LD      (FCB+14),HL
              LD      HL,0
              LD      (FCB+33),HL
              LD      (FCB+35),HL
              LD      DE,FCB           ; escribe bloque ram
              LD      HL,1
	      dos     ESCR_BLOCK
              OR      A
              JR      Z,_OK1
              LD      A,2              ; no se pudo escribir
              RET
_OK1:	      LD      DE,FCB
	      dos     CERRAR_FICH
              RET


;****************************************************************************
; BinaryFileRead
;  Lee un fichero binario a DMA 
; -> HL nombre fichero en formato fcb, DE  direccion destino, BC bytes a leer
;  <- A=#ff,  HL num bytes leidos, A=1 : no se pudo abrir_
;  REGS: Todos
;  CALLS: Bdos
;  OBS:
;****************************************************************************
BinaryFileRead:
              ; --- Copia nombre de fichero a FCB ---
	      PUSH    bc
	      push    de 
	      XOR     A
              LD      (FCB),A          ; unidad activa, primer byte 0
              LD      DE,FCB+1
              LD      BC,11
              LDIR

              ; --- Abre el fichero ---

              LD      DE,FCB
	      dos     ABRIR_FICH
              POP     DE               ; destino
              OR      A
              JR      Z,_FREAD

              LD      A,1              ; Codigo de error: No se pudo abrir

	      pop     bc
              RET

              ; --- Lectura del fichero ---
_FREAD:
	      dos     SET_DMA	       ; de=nuevo dma
              LD      HL,1
              LD      (FCB+14),HL      ; tamanyo registro
              LD      HL,0
              LD      (FCB+33),HL
              LD      (FCB+35),HL      ; registro aleatorio 0
              LD      DE,FCB           ; lee bloque ram
	      pop     hl	       ; 64 kb, (materialmente imposible)
	      dos     LEER_BLOCK
              RET


