;-----------------------------------------------------------------------------
; FUNCIONES DE GESTION DE MEMORIA MAPEADA		    1998 ENRIC GEIJO
;-----------------------------------------------------------------------------
;  Modulo para la gesti�n de N(=4 de momento) mapeadores al mismo tiempo
;  Compatible solo DOS1, (posible futura extensi�n DOS2)

	psect	code,global
	psect	data,global
	global	SetPage0,SetPage1,SetPage2,InitMem   

	;---------------------------------------------------------------------
	; InitMem
	;	Detecta todos los mapeadores e inicializa las tablas para el
	;	uso de las funciones del m�dulo.
	;---------------------------------------------------------------------
	psect	code
InitMem:
		;---------------------------
		; Guarda p�ginas de sistema
		;---------------------------
	in	a,(0fch)
	ld	(Sys0P),a
	in	a,(0ffh)
	ld	(Sys3P),a
	in	a,(0a8h)	; slot activo en P0=slot activo en P3
	and	3
	ld	(SysMS),a		 

		;---------------------------
		; Guarda p�ginas activas
		;---------------------------
	in	a,(0fch)
	ld	(CpyP0),a
	in	a,(0fdh)
	ld	(CpyP1),a
	in	a,(0feh)
	ld	(CpyP2),a
	in	a,(0ffh)
	ld	(CpyP3),a
	
		;---------------------------
		; Busca RAM mapeada 
		;---------------------------
	; de momento, a falta de referencias en test de memoria
	; activo SOLO EL MAPEADOR PRIMARIO		  
	; (se asume que se ejecuta desde el DOS)
	ld	a,1	   
	ld	(nmapers),a	   
	in	a,(0a8h)
	and	00000011B
	ld	(slotM1),a	  
	ld	a,7		;128Kb	permite
	ld	(nsegM1),a				 
	ret

	;---------------------------------------------------------------------
	; SetPage0
	;      coloca en la pagina f�sica 0 la p�gina l�gica A del mapeador B
	;      (no comprueba desbordamiento de p�gina)
	;	NOTA :: Ninguna de las funciones siguientes comprueba si la
	;		p�gina a activar es de sistema --> hacer externamente.
	;---------------------------------------------------------------------	     
	psect	code
SetPage0:
	di
	ld	(CpyP0),a
	out	(0fch),a
	ld	hl,MapTab	 
	ld	a,b
	sla	a
	ld	c,a
	ld	b,0
	add	hl,bc
	ld	a,(hl)		; slot del mapeador
	ld	b,a
	in	a,(0a8h)
	and	11111100B
	or	b
	out	(0a8h),a	; activa el slot			
	ret

	;---------------------------------------------------------------------
	; SetPage1
	;      coloca en la pagina f�sica 0 la p�gina l�gica A del mapeador B
	;      (no comprueba desbordamiento de p�gina) 
	;---------------------------------------------------------------------	     
	psect	code
SetPage1:
	di
	ld	(CpyP1),a
	out	(0fdh),a
	ld	hl,MapTab	 
	ld	a,b
	sla	a
	ld	c,a
	ld	b,0
	add	hl,bc
	ld	a,(hl)		; slot del mapeador
	rl	a
	rl	a
	ld	b,a	   
	in	a,(0a8h)
	and	11110011B
	or	b
	out	(0a8h),a	; activa el slot		 
	ret

	;---------------------------------------------------------------------
	; SetPage2
	;      coloca en la pagina f�sica 0 la p�gina l�gica A del mapeador B
	;      (no comprueba desbordamiento de p�gina)
	;---------------------------------------------------------------------	     
	psect	code
SetPage2:
	di
	ld	(CpyP2),a
	out	(0feh),a
	ld	hl,MapTab	 
	ld	a,b
	sla	a
	ld	c,a
	ld	b,0
	add	hl,bc
	ld	a,(hl)		; slot del mapeador
	rl	a
	rl	a
	rl	a
	rl	a
	ld	b,a	   
	in	a,(0a8h)
	and	11001111B
	or	b
	out	(0a8h),a	; activa el slot		   
	ret


	;--------------------------------------------------------------------
	; Tabla de variables de mapeador
	;--------------------------------------------------------------------		 
	psect	data
MapTab:
nmapers:		; numero de mapeadores 
	defb	  0
slotM1: defb	  0	  ; mapeador 0
nsegM1: defb	  0	  
slotM2: defb	  0	  ; mapeador 1
nsegM2: defb	  0
slotM3: defb	  0	  ; mapeador 2
nsegM3: defb	  0
slotM4: defb	  0	  ; mapeador 3
nsegM4: defb	  0

	;---------------------------------------------------------------------			
	; Tabla de p�ginas activas
	;--------------------------------------------------------------------
	psect	data
CpyP0:	defb	  0
CpyP1:	defb	  0
CpyP2:	defb	  0
CpyP3:	defb	  0
	
	;---------------------------------------------------------------------
	; Tabla de p�ginas del sistema (aquellas activas en P0 y P3
	;	al iniciar la ejecuci�n
	;---------------------------------------------------------------------
Sys0P:	defb	  0	  ; paginas
Sys3P:	defb	  0	  
SysMS:	defb	  0	  ; slot de mapeador de sistema 

